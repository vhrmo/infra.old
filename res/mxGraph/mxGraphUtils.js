/*
 * Utility functions for working with mxGraph
 */


let mxGraphUtils = {

    /**
     * Loads a single stencils file
     * @param stencilFileName - file name within the stencils subfolder of the mxBasePath without an extension
     */
    loadStencil: function (stencilFileName) {
        let req = mxUtils.load(mxBasePath + '/stencils/' + stencilFileName + '.xml');
        let root = req.getDocumentElement();
        let shape = root.firstChild;
        let stencilGroup = stencilFileName.replace(/\//g, ".");

        while (shape != null) {
            if (shape.nodeType === mxConstants.NODETYPE_ELEMENT) {
                let stencilName = shape.getAttribute('name');
                stencilName = stencilName.toLowerCase().replace(/ /g, "_");
                mxStencilRegistry.addStencil('mxgraph.' + stencilGroup + '.' + stencilName, new mxStencil(shape));
            }

            shape = shape.nextSibling;
        }
    },


    exportGraphToXml: function (graph) {
        let encoder = new mxCodec();
        let node = encoder.encode(graph.getModel());
        mxUtils.popup(mxUtils.getPrettyXml(node), true);
    }

}