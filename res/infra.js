let clusterServers = [];
let edges = [];
let showConnections = true;
let pageDescription = 'Infra';

$(document).ready(function () {
    $("#nav-placeholder").load("navbar.html");

    if (!mxClient.isBrowserSupported()) {
        mxUtils.error('Browser is not supported!', 200, false);
        return;
    }

    let cluster = GetURLParameter('cluster');
    let env = GetURLParameter('env');

    pageDescription = cluster + ' ' + env + ' infrastructure';
    document.getElementById("h1").innerHTML = pageDescription;
    document.title = pageDescription;

    servers.forEach(function (entry) {
        if (entry["cluster"] === cluster && entry['environment'] === env) {
            clusterServers.push(entry);
        }
    });

    mxGraphUtils.loadStencil('citrix');
    mxGraphUtils.loadStencil('azure');
    mxGraphUtils.loadStencil('ios7/icons');
    mxGraphUtils.loadStencil('office/concepts');
    let graph = new mxGraph(document.getElementById('graphContainer'));
    buildGraph(graph);

    $("#hideConnections").click(function () {
        graph.getModel().beginUpdate();
        try {
            showConnections = !showConnections;
            edges.forEach(function (item) {
                item.setVisible(showConnections);
            });
            graph.refresh();
        } finally {
            graph.getModel().endUpdate();
        }
        $("#hideConnections").text(showConnections ? "Hide connections" : "Show connections");

    });

    $("#export").click(function () {
        let encoder = new mxCodec();
        let node = encoder.encode(graph.getModel());
        this.href = 'data:text/plain;charset=utf-8,' + encodeURIComponent(mxUtils.getPrettyXml(node));
        this.download = pageDescription + '.xml';
        //mxUtils.getPrettyXml(node)
        // mxGraphUtils.exportGraphToXml(graph);
    });

    function buildGraph(graph) {
        let parent = graph.getDefaultParent();
        // graph.ordered = false;
        graph.keepEdgesInBackground = true;

        // Disables basic selection and cell handling
        graph.setEnabled(false);

        // Highlights the vertices when the mouse enters
        new EdgeHighlighter(graph, 'blue');

        // Adds cells to the model in a single step
        graph.getModel().beginUpdate();
        try {

            let stylePrefix = 'verticalLabelPosition=middle;verticalAlign=top;rounded=1;arcSize=5;spacing=0;fillColor=lightgray;strokeColor=lightgray;foldable=0;fillOpacity=20';

            // Create zone containers + gaps to be placed between them
            let internetZone = graph.insertVertex(parent, null, '', 0, 0, 20, 20, 'strokeColor=none;fillColor=none;foldable=0');
            let internalUsersZone = graph.insertVertex(parent, null, '', 0, 0, 10, 10, 'strokeColor=none;fillColor=none;foldable=0');
            let gap1 = graph.insertVertex(parent, null, '', 0, 0, 10, 10, 'strokeColor=none;fillColor=none;foldable=0');
            let gap2 = graph.insertVertex(parent, null, '', 0, 0, 10, 10, 'strokeColor=none;fillColor=none;foldable=0');
            let gap3 = graph.insertVertex(parent, null, '', 0, 0, 10, 10, 'strokeColor=none;fillColor=none;foldable=0');
            let gap4 = graph.insertVertex(parent, null, '', 0, 0, 10, 10, 'strokeColor=none;fillColor=none;foldable=0');
            let dmzZone1 = graph.insertVertex(parent, null, 'eShelter - W1-R zone', 0, 200, 400, 200, stylePrefix);
            let dmzZone2 = graph.insertVertex(parent, null, 'Equinix - W1-R zone', 0, 400, 400, 200, stylePrefix);
            let devZone1 = graph.insertVertex(parent, null, 'eShelter - D2-NR zone', 0, 600, 400, 200, stylePrefix);
            let appZone1 = graph.insertVertex(parent, null, 'eShelter - A2-R zone', 0, 600, 400, 200, stylePrefix);
            let appZone2 = graph.insertVertex(parent, null, 'Equinix - A2-R zone', 0, 800, 400, 200, stylePrefix);
            let devZone2 = graph.insertVertex(parent, null, 'Equinix - D2-NR zone', 0, 800, 400, 200, stylePrefix);

            // Connect zones to build a hierarchical structure
            let invisibleEdgeStyle = 'strokeColor=none';
            graph.insertEdge(parent, null, '', internetZone, dmzZone1, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internetZone, gap1, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internetZone, dmzZone2, invisibleEdgeStyle);

            graph.insertEdge(parent, null, '', dmzZone1, gap2, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', dmzZone1, internalUsersZone, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', gap1, internalUsersZone, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', dmzZone2, internalUsersZone, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', dmzZone2, gap3, invisibleEdgeStyle);

            graph.insertEdge(parent, null, '', internalUsersZone, devZone1, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', gap2, appZone1, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internalUsersZone, appZone1, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internalUsersZone, gap4, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internalUsersZone, appZone2, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', gap3, appZone2, invisibleEdgeStyle);
            graph.insertEdge(parent, null, '', internalUsersZone, devZone2, invisibleEdgeStyle);

            // Add servers to the graph
            let extWebServers1 = addServers(dmzZone1, 'Web', 'ESH-W1-R', 'mxgraph.citrix.web_server');
            let extWebServers2 = addServers(dmzZone2, 'Web', 'EQX-W1-R', 'mxgraph.citrix.web_server');
            let intWebServers1 = addServers(appZone1, 'Web', 'ESH-A2-R', 'mxgraph.citrix.web_server');
            let intWebServers2 = addServers(appZone2, 'Web', 'EQX-A2-R', 'mxgraph.citrix.web_server');
            let appServers1 = addServers(appZone1, 'App', 'ESH-A2-R', 'mxgraph.citrix.tower_server');
            let appServers2 = addServers(appZone2, 'App', 'EQX-A2-R', 'mxgraph.citrix.tower_server');
            let devWebServers1 = addServers(devZone1, 'Web', 'ESH-D2-NR', 'mxgraph.citrix.web_server');
            let devWebServers2 = addServers(devZone2, 'Web', 'EQX-D2-NR', 'mxgraph.citrix.web_server');
            let devServers1 = addServers(devZone1, 'App', 'ESH-D2-NR', 'mxgraph.citrix.tower_server');
            let devServers2 = addServers(devZone2, 'App', 'EQX-D2-NR', 'mxgraph.citrix.tower_server');

            // Hide empty zones
            dmzZone1.setVisible(dmzZone1.getChildCount() > 0);
            dmzZone2.setVisible(dmzZone2.getChildCount() > 0);
            appZone1.setVisible(appZone1.getChildCount() > 0);
            appZone2.setVisible(appZone2.getChildCount() > 0);
            devZone1.setVisible(devZone1.getChildCount() > 0);
            devZone2.setVisible(devZone2.getChildCount() > 0);

            // Hide unnecessary gaps
            gap1.setVisible(dmzZone1.isVisible() && dmzZone2.isVisible());
            gap2.setVisible(appZone1.isVisible() && appZone2.isVisible());
            gap3.setVisible(appZone1.isVisible() && appZone2.isVisible());
            gap4.setVisible(appZone1.isVisible() && appZone2.isVisible());

            if (dmzZone1.isVisible() || dmzZone2.isVisible()) {
                let externalUsers = insertShape(internetZone, null, 'External users', 'verticalLabelPosition=bottom;verticalAlign=top;perimeterSpacing=30', 'mxgraph.ios7.icons.globe', 40);
                if (dmzZone1.isVisible()) {
                    let externalLB1 = insertLB(dmzZone1, null, 'External LB');
                    connectNodes([externalUsers], [externalLB1]);
                    connectNodes([externalLB1], extWebServers1);
                    connectNodes(extWebServers1, appServers1);
                }
                if (dmzZone2.isVisible()) {
                    let externalLB2 = insertLB(dmzZone2, null, 'External LB');
                    connectNodes([externalUsers], [externalLB2]);
                    connectNodes([externalLB2], extWebServers2);
                    connectNodes(extWebServers2, appServers2);
                }
            }

            let internalUsers = null;

            // are there internal web servers? if yes - add internal users, LB and the servers
            if (intWebServers1.length > 0 || intWebServers2.length > 0) {
                if (internalUsers === null) {
                    internalUsers = insertShape(internalUsersZone, null, 'Internal users', 'verticalLabelPosition=bottom;verticalAlign=top;perimeterSpacing=20', 'mxgraph.ios7.icons.globe', 40);
                }
                if (intWebServers1.length > 0 ) {
                    let internalLB1 = insertLB(appZone1, null, 'Internal LB');
                    connectNodes([internalUsers], [internalLB1]);
                    connectNodes([internalLB1], intWebServers1);
                    connectNodes(intWebServers1, appServers1);
                }
                if (intWebServers2.length > 0 ) {
                    let internalLB2 = insertLB(appZone2, null, 'Internal LB');
                    connectNodes([internalUsers], [internalLB2]);
                    connectNodes([internalLB2], intWebServers2);
                    connectNodes(intWebServers2, appServers2);
                }
            }

            if (devWebServers1.length > 0 || devWebServers2.length > 0) {
                if (internalUsers === null) {
                    internalUsers = insertShape(internalUsersZone, null, 'Internal users', 'verticalLabelPosition=bottom;verticalAlign=top;perimeterSpacing=20', 'mxgraph.ios7.icons.globe', 40);
                }
                if (devWebServers1.length > 0 ) {
                    connectNodes([internalUsers], devWebServers1);
                    connectNodes(devWebServers1, devServers1);
                }
                if (devWebServers2.length > 0 ) {
                    connectNodes([internalUsers], devWebServers2);
                    connectNodes(devWebServers2, devServers2);
                }
            }

            function addServers(parent, type, zone, shape) {
                let _servers = [];
                let stylePrefix = 'verticalLabelPosition=bottom;verticalAlign=top;perimeterSpacing=5;labelBackgroundColor=whitesmoke;textOpacity=80';

                clusterServers.forEach(function (server) {
                    let desc = server['description'];
                    if (desc.indexOf(type) !== -1 && server['zone'] === zone) {
                        let s = insertShape(parent, null, server['type'] + '\n' + server['name'] + '\n' + server['ip'], stylePrefix, shape);
                        _servers.push(s);
                    }
                });
                return _servers;
            }

            function insertLB(parent, id, value) {
                return insertShape(parent, null, value, 'verticalLabelPosition=bottom;verticalAlign=top;perimeterSpacing=10;labelBackgroundColor=whitesmoke;textOpacity=80;fillColo=white', 'mxgraph.azure.load_balancer_generic', 30);
            }

            function insertShape(parent, id, value, stylePrefix, shape, height) {
                let stencil = mxStencilRegistry.getStencil(shape);
                let ratio = stencil.w0 / stencil.h0;
                height = height || 60;
                return graph.insertVertex(parent, id, value, 0, 0, height * ratio, height, stylePrefix + ';shape=' + shape);
            }

            function connectNodes(nodes1, nodes2) {
                nodes1.forEach(function (n1) {
                    nodes2.forEach(function (n2) {
                        edges.push(graph.insertEdge(parent, null, '', n1, n2, 'strokeColor=lightgray;curved=0;dx=0'));
                        // edgeStyle=segmentEdgeStyle;
                        // edgeStyle=orthogonalEdgeStyle;
                    })
                })
            }

            function layoutZone(parentNode) {
                var layout = new mxHierarchicalLayout(graph);
                layout.resizeParent = true;
                layout.parentBorder = 50;
                layout.intraCellSpacing = 80; // medzera medzi servrami vedla seba
                layout.interRankCellSpacing = 60; // medzera medzi servrami nad sebou
                // layout.interHierarchySpacing = 120;
                layout.execute(parentNode);
            }

            function layoutUserZone(parentNode) {
                var layout = new mxHierarchicalLayout(graph);
                layout.resizeParent = true;
                layout.parentBorder = 10;
                // layout.intraCellSpacing = 80; // medzera medzi servrami vedla seba
                // layout.interRankCellSpacing = 60; // medzera medzi servrami nad sebou
                // layout.interHierarchySpacing = 120;
                layout.execute(parentNode);
            }

            function layoutGraph(parentNode) {
                var layout = new mxHierarchicalLayout(graph);
                layout.traverseAncestors = false;
                layout.interRankCellSpacing = 20; // medzera medzi blokmi nad sebou
                layout.execute(parentNode);
            }


            function doHierarchicalLayout() {
                layoutUserZone(internetZone);
                layoutUserZone(internalUsersZone);
                layoutZone(dmzZone1);
                layoutZone(dmzZone2);
                layoutZone(appZone1);
                layoutZone(appZone2);
                layoutZone(devZone1);
                layoutZone(devZone2);
                layoutGraph(parent);
            }

            doHierarchicalLayout();
        } finally {
            // Updates the display
            graph.getModel().endUpdate();
        }
    }

});

/*******************************************************************
 * Edge highlighter
 *
 * Highlight all edges of a cell (inbound and outbound)
 *******************************************************************/
function EdgeHighlighter(graph, color) {
    mxCellMarker.call(this, graph, color, color, 1);
    this.graph.addMouseListener(this);

    // Automatic deallocation of memory
    if (mxClient.IS_IE) {
        mxEvent.addListener(window, 'unload', mxUtils.bind(this, function () {
            this.destroy();
        }));
    }
};

/**
 * Extends mxCellMarker.
 */
mxUtils.extend(EdgeHighlighter, mxCellMarker);

/**
 * Variable: currentState
 *
 * Holds the marked <mxCellState>.
 */
EdgeHighlighter.prototype.currentState = null;

/**
 * Variable: currentHighlights
 *
 * Holds an array of <mxCellHighlight> objects for each highlighted edge.
 */
EdgeHighlighter.prototype.currentHighlights = null;


EdgeHighlighter.prototype.highlightEdges = function (state) {
    if (state !== null) { // anything selected?
        this.currentHighlights = [];
        let cell = state.cell;
        if (cell.getChildCount() === 0 && cell.getValue() !== '') {
            if (cell.getEdgeCount() > 0) { // highlight all edges
                for (let i = cell.getEdgeCount() - 1; i >= 0; --i) {
                    let highlight = new mxCellHighlight(this.graph, this.validColor, 1);
                    this.currentHighlights.push(highlight);
                    let edge = cell.getEdgeAt(i);
                    highlight.highlight(this.graph.view.getState(edge));
                }
            }
        }
    }
};


EdgeHighlighter.prototype.destroyEdgeHighlights = function () {
    if (this.currentHighlights !== null) {
        this.currentHighlights.forEach(function (item) {
            item.destroy();
        });
        this.currentHighlights = null;
    }
};

EdgeHighlighter.prototype.mouseDown = function (sender, me) {
};

/**
 * Function: mouseMove
 *
 * Handles the event by highlighting the cell under the mousepointer if it
 * is over the hotspot region of the cell.
 */
EdgeHighlighter.prototype.mouseMove = function (sender, me) {
    if (this.isEnabled()) {
        // this.process(me);

        // translate mxMouseEvent to a mxCellState
        let state = this.getState(me);
        // this.setCurrentState(state, me);

        if (this.currentState !== state) {
            this.destroyEdgeHighlights();
            this.currentState = state;
            this.highlightEdges(state);
        }
    }
};

EdgeHighlighter.prototype.mouseUp = function (sender, me) {
};

/**
 * Function: destroy
 *
 * Destroys the object and all its resources and DOM nodes. This doesn't
 * normally need to be called. It is called automatically when the window
 * unloads.
 */
EdgeHighlighter.prototype.destroy = function () {
    if (!this.destroyed) {
        this.destroyed = true;

        this.graph.removeMouseListener(this);
        mxCellMarker.prototype.destroy.apply(this);
    }
};

