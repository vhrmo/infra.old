
if (typeof jQuery === 'undefined') {
    console.error('Missing jQuery module.');
}

if (typeof login === "undefined") {
    console.error('Missing login module. Requires login.js');
}


/*

https://appviewx.1dc.com/AppViewXNGWeb/executeWorkflowAssociateScript.do




 */



var appviewx = {

    data: {},


    ajaxSettings: {
        type : 'POST',
        // xhrFields: {
        //     withCredentials: true
        // },
        // beforeSend: function (xhr) {
        //     xhr.setRequestHeader("Authorization", "Basic " + btoa(login.uname + ":" + login.psw));
        // },
        // error: function(data){
        //     console.error('Cannot load AppViewX data');
        //     console.error(data);
        // },
    }



}

appviewx.APIPath = '/AppViewXNGWeb/'; // used with node http-server - tests locally
var hname = window.location.hostname;
if (hname.includes('1dc.com')) {
    appviewx.APIPath = '/infra/appviewx/'; // used with Apache where jira rest is mapped to this path
}
console.log('AppViewX API path: ' + appviewx.APIPath);

// console.log(window.btoa(unescape(encodeURIComponent(pwd))));

appviewx.loadData = function(url, callback) {
    var t = this;

    url = 'AppViewXNGWeb/executeWorkflowAssociateScript.do';

    data = {
        "task_id": "createform_1",
        "fieldData": [
            {
                "labelName": "MODIFY VIP",
                "values": "TLS Settings",
                "fieldId": "dd_modify_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Dropdown",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "Load Balancer IP",
                "values": "10.83.13.105",
                "fieldId": "txt_lb_unit",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "VIP NAME",
                "values": "ONLINE-QA-FIRSTDATA-COM-VIP",
                "fieldId": "txt_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": " FIND VIP",
                "values": "",
                "fieldId": "btn_find_vip",
                "group": "REQUEST DETAILS",
                "elementType": "Button",
                "parent": "",
                "mandatory": false
            },
            {
                "labelName": "ENTER VIP Address Port",
                "values": "443",
                "fieldId": "txt_vip_port",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "ENTER VIP Address",
                "values": "10.72.66.31",
                "fieldId": "txt_vip_addr",
                "group": "REQUEST DETAILS",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "INSTRUCTIONS",
                "values": "1.- Complete ALL mandatory fields\n2.- Use the virtual IP address and port to search for the VIP address Name\n3.- Once the request is submitted, the template will generate the configuration and apply them immediately.\n\nIf you need assistance please send email to SAS-Consulting@firstdata.com",
                "fieldId": "txt_ins",
                "group": "",
                "elementType": "Multi-line",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "User ID",
                "values": "tsku1290@1dc.com",
                "fieldId": "txt_userid",
                "group": "CONTACT INFORMATION",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "ENTER First Name",
                "values": "",
                "fieldId": "txt_user_name",
                "group": "CONTACT INFORMATION",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "ENTER Last Name",
                "values": "",
                "fieldId": "txt_user_last",
                "group": "CONTACT INFORMATION",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "labelName": "ENTER Email",
                "values": "",
                "fieldId": "txt_user_email",
                "group": "CONTACT INFORMATION",
                "elementType": "Text box",
                "parent": "",
                "mandatory": true
            },
            {
                "values": "tsku1290@1dc.com",
                "fieldId": "appviewx_username",
                "mandatory": true
            }
        ],
        "workflowName": "LB - Modify Load balancing VIP",
        "scriptName": "get_tls_config",
        "aclFilter": "None",
        "flowName": null,
        "requestCategory": "default"
    };


    $.ajax({
        type: "POST",
        url: appviewx.APIPath + "authenticate.do",
        contentType: "application/json",
        dataType: "json",
        headers: {
            // "Referer": 'login.do',
            // "X-Alt-Referer": '/AppViewXNGWeb/login.do',
            // "X-Requested-With": 'XMLHttpRequest',
        },
        data: JSON.stringify({
            "username": login.uname,
            "password": btoa(login.psw)
        })
    })
        .done(function (data) {
            console.log("success", data);
        })
        .fail(function (jqXhr, textStatus) {
            console.error("fail", jqXhr, textStatus);
        });
}




$(document).ready(function() {
    console.log("I'm in");

    login.callback = loadData;
    login.getCredentials();
});

// Callback when credentials are enterred by the user
function loadData() {
    console.log("Load data");
    appviewx.loadData("", null);
}

