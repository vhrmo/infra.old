/*
 * Object to keep variables for graph building
 */
var mermaidJSON = {
    id_seq: 1,
    graphConfig: "graph LR\n",

    config: {},
    zones: {},
    nats: {},
    dr_nats: {},
    classes: {},
    connections: []

};

const supportsCustomElementsV1 = 'customElements' in window;

mermaidJSON.addClassDefinition = function (e) {
    this.graphConfig += "classDef " + e.name + " " + e.style + "\n";
}

mermaidJSON.addGraphElement = function (parent, e) {
    // inherit zone from parent
    if (!e.zone && parent && parent.zone) e.zone = parent.zone;
    // inherit type from parent
    if (!e.type && parent && parent.type) e.type = parent.type;

    if (!e.id) { // if component id not explicitly defined
        // FD Net zone special treatment
        if (e.zone === "FD Net") {
            e.id = ++this.id_seq;
            // treat FD NET zone IPs as if they are NAT-ed to the same IP in eShelter / Equinix
            e.internal_nat = e.ip;
        } else if (e.ip) {
            e.id = e.ip;
        } else {
            e.id = ++this.id_seq;
        }
    }

    // Set properties based on the component type
    if (e.warning && !e.class) e.class = "warning-node";
    if (e.error && !e.class) e.class = "error-node";
    if (e.type) {
        if (!e.icon) e.icon = "ico-" + e.type;
        if (!e.class) e.class = e.type;
    }

    let popover = null;

    // set load balancer properties
    if (e.deviceType === 'load-balancer') {
        // e.class = e.deviceType;
        e.icon = "";
    } else if (e.deviceType === 'web-server') {
    }

    let renderer = null;
    if (this.config.renderers) {
        let renderer = this.config.renderers[e.deviceType];
        if (!renderer) renderer = this.config.renderers["default"];

        if (renderer && renderer.renderPopover) {
            popover = this.config.renderers[e.deviceType].renderPopover(e);
        }
    }



    var graphElement = e.id + "(<div class='node-label-" + e.class + "'";
    if (popover) {
        graphElement += " data-container='body' data-html='true' data-toggle='popover' data-trigger='hover' data-content='";
        graphElement += popover;
        graphElement += "'";
    }
    graphElement += ">";

    graphElement += "<div class='node-title'>";
    if (e.deviceType) {
        if (e.deviceType === 'load-balancer') {
            // put load balancer VIP into the content of the x-load-balancer tag
            graphElement += "<x-" + e.deviceType + ">" + e.ip + "</x-" + e.deviceType + ">";
        }
    }
    if (e.name) {
        graphElement += "<b>" + e.name + "</b>";
        graphElement += "<br>";
    }
    var url = 'unknown.url';
    if (e.url) {
        var urls = e.url.split(','); // allow for comma separated list of urls
        url = urls[0];
        if (supportsCustomElementsV1) {
            var params = '';
            if (e.zone === 'Internet') params += ' sslLabs ';
            urls.forEach(function(entry) {
                graphElement += "<x-url" + params + ">" + entry + "</x-url><br>";
            });
        }
        else {
            urls.forEach(function(entry) {
                graphElement += "<b>" + entry + "</b><br>";
            });
        }
    }
    if (e.ip) {
        if (supportsCustomElementsV1) graphElement += "<x-ip>" + e.ip + "</x-ip><br>";
        else graphElement += e.ip + "<br>";
    }
    graphElement += "</div>";

    if (e.comment) graphElement += "<div class='comment'>" + e.comment + "</div>";
    if (e.contexts) graphElement += "<x-contexts baseUrl='" + url + "'>" + e.contexts + "</x-contexts>";
    if (e.icon) graphElement += "<"+e.icon+"></"+e.icon+">";
    if (e.warning) {
        if (supportsCustomElementsV1) graphElement += "<x-warning>"+e.warning+"</x-warning>";
        else graphElement += "<br>!!!! Warning: "+e.warning+" !!!!<br>";
    }
    if (e.error) {
        if (supportsCustomElementsV1) graphElement += "<x-error>"+e.error+"</x-error>";
        else graphElement += "<br>!!!! Error: "+e.error+" !!!!<br>";
    }
    graphElement += "</node-label-"+e.class+">)";
    graphElement += "\n";

    // register NATs for later linking
    if (e.internal_nat) {
        if (this.nats[e.internal_nat]) {
            // adding new NAT for existing target IP
            this.nats[e.internal_nat].push(e);
        } else {
            // NAT for target IP doesn't exist yet
            this.nats[e.internal_nat] = [e];
        }
    }

    // register DR NATs in case RHI is used for failover for later linking
    if (e.dr_internal_nat) {
        if (this.dr_nats[e.dr_internal_nat]) {
            // adding new NAT for existing target IP
            this.dr_nats[e.dr_internal_nat].push(e);
        } else {
            // NAT for target IP doesn't exist yet
            this.dr_nats[e.dr_internal_nat] = [e];
        }
    }

    // store class info - flushed at the end of the graph
    if (e.class) {
        if (this.classes[e.class]) {
            this.classes[e.class].push(e);
        } else {
            this.classes[e.class] = [e];
        }
    }

    if (e.zone) { // add element to zone
        if (this.zones[e.zone]) {
            this.zones[e.zone] += graphElement;
        } else {
            this.zones[e.zone] = graphElement;
        }
    } else { // add standalone element
        this.graphConfig += graphElement;
    }
}

mermaidJSON.addGraphConnection = function(a, b) {
    if (b.link_note) {
        // this.connections += a.id + "-." + b.link_note + ".->" + b.id + "\n";
        this.connections.push({
            source: a.id,
            target: b.id,
            line: 'dotted',
            text: b.link_note
        })
    } else {
        // this.connections += a.id + "-->" + b.id + "\n";
        this.connections.push({
            source: a.id,
            target: b.id,
        })
    }
}

mermaidJSON.addLink = function(parent, link) {
    if (!parent.id) {
        console.error("Missing parent object ID:", parent);
        return;
    }
    if (link.ip && !link.id) link.id = link.ip;
    if (!link.id) {
        console.error("Missing target object ID:", link);
        return;
    }
    link.source = parent.id;
    link.target = link.id;
    this.connections.push(link);
}

mermaidJSON.addLinks = function(parent, links) {
    if (!parent.id) {
        console.error("Missing parent object ID:", parent);
        return;
    }
    var mthis = this;
    links.forEach(function (e) {
        mthis.addLink(parent, e);
    });
}


/*
 * Flush zones to the graphConfig
 */
mermaidJSON.flushZones = function() {
    // flush zones in reverse order - they come in original order on the graph
    var t = "";
    for (zone in this.zones) {
        t = "subgraph " + zone + "\n" + this.zones[zone] + "end\n" + t;
    }
    this.graphConfig += t;
}

/*
 * Flush NAT links to the graphConfig
 */
mermaidJSON.flushNATs = function() {
    var mthis = this;
    for (nat in this.dr_nats) {
        this.dr_nats[nat].forEach(function (s) {
            mthis.graphConfig += s.id + "-.->" + nat + "\n";
        })
    }
    for (nat in this.nats) {
        this.nats[nat].forEach(function (s) {
            mthis.graphConfig += s.id + "-->" + nat + "\n";
        })
    }
}

/*
 * Flush CSS styling to the graphConfig
 */
mermaidJSON.flushClasses = function() {
    var mthis = this;
    for (var c in this.classes) {
        this.graphConfig += "class ";
        this.classes[c].forEach(function (e) {
            mthis.graphConfig += e.id + ",";
        });
        this.graphConfig += " " + c + "\n";
    }
}

/*
 * Flush connections to the graphConfig
 */
mermaidJSON.flushConnections = function() {
    var result = "";
    this.connections.forEach(function (e) {
        var linkType = "-->";
        if (e.line === "dotted") {
            linkType = "-.->"
        }

        result += e.source + linkType;
        if (e.text) {
            result += "|" + e.text + "|";
        }
        result += e.target+"\n";
    });

    this.graphConfig += result;
}


mermaidJSON.generateGraph = function(graphDef, config) {
    /*
     * Process JSON configuration
     */
    if (config) this.config = config;
    else {
        // default configuration
    }

    var mthis = this;
    if (graphDef.classes) {
        graphDef.classes.forEach(function (e) {
            mthis.addClassDefinition(e);
        });
    }
    graphDef.nodes.forEach(function (e) {
        mthis.addGraphElement(null, e);
        if (e.servers) {
            e.servers.forEach(function (s) {
                mthis.addGraphElement(e, s);
                mthis.addGraphConnection(e, s);
            })
        } else if (e.link) {
            mthis.addLink(e, e.link);
        } else if (e.links) {
            // just create links between component without actually adding the component
            mthis.addLinks(e, e.links);
        } else {
            if (e.primary) {
                mthis.addGraphElement(e, e.primary);
                mthis.addGraphConnection(e, e.primary);
            }
            if (e.dr) {
                mthis.addGraphElement(e, e.dr);
                mthis.addGraphConnection(e, e.dr);
            }
        }
    });

    this.flushZones();
    this.flushConnections();
    this.flushNATs();
    this.flushClasses();
    return this.graphConfig;
}


