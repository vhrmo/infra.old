var login = {
    id: "login-form",
    form: null,
    uname: "",
    psw: "",

};

// add login form to the document DOM
login.createForm = function () {
    var form = document.createElement("div");
    form.id = this.id;
    form.className = "login-modal";
    form.innerHTML =
        "<form class='login-modal-content login-animate' method='POST' onsubmit='event.preventDefault(); login.login();'>" +
        "    <div class='logincontainer'>" +
        "        <label for='uname'><b>Username</b></label>" +
        "        <span onclick='login.hideLoginForm()' class='login-close' title='Close'>&times;</span>" +
        "        <input type='text' placeholder='Enter Username' id='uname' name='uname' autofocus required>" +
        "        <label for='psw'><b>Password</b></label>" +
        "        <input type='password' placeholder='Enter Password' id='psw' name='psw' required>" +
        "        <button class='loginbtn' type='submit'>Login</button>" +
        "    </div>" +
        "    <div class='logincontainer' style='background-color:#f1f1f1'>" +
        "        <button type='button' onclick='login.hideLoginForm()' class='cancelbtn'>Cancel</button>" +
        "    </div>\n" +
        "</form>"
    document.body.appendChild(form);
    this.form = form;
}

// show the login form
login.showLoginForm = function () {
    if (this.form === null) {
        this.createForm();
    }

    var modal = this.form;
    modal.style.display = 'block';

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function(event) {
        if (event.target === modal) {
            modal.style.display = "none";
        }
    }
}

// hide the form
login.hideLoginForm = function () {
    this.form.style.display = 'none';
}

// pressed the login button or form is submitted
login.login = function () {
    this.uname = this.form.children[0].uname.value;
    this.psw = this.form.children[0].psw.value;
    // store info to session storage
    if (typeof(Storage) !== "undefined") {
        sessionStorage.uname = this.uname;
        sessionStorage.psw = this.psw;
    }
    this.hideLoginForm();
    console.log('Calling callback');
    this.callback();
}

// Get the credentials from session storage.
// If not available there - open modal login dialog.
// Define callback function to get called once credentials are obtained.
login.getCredentials = function() {
    if (typeof(Storage) !== "undefined") {
        // Code for localStorage/sessionStorage.
        var credentialsCount = 0;
        if (sessionStorage.uname) {
            this.uname = sessionStorage.uname;
            credentialsCount += 1;
        }
        if (sessionStorage.psw) {
            this.psw = sessionStorage.psw;
            credentialsCount += 1;
        }
        if (credentialsCount < 2) {
            this.showLoginForm();
        } else {
            this.callback();
        }
    } else {
        this.showLoginForm();
    }
}

// Override this function to get called once credentials are obtained from user
login.callback = function() {
    console.log('No callback method is defined');
}
