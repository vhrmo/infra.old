let lbScripts = [
  "ag.prod.lb.js",
  "ag.sit.lb.js",
  "ag.uat.lb.js",
  "misc.prod.lb.js",
  "misc.uat.lb.js",
  "misc.sit.lb.js",
  "mop.prod.lb.js",
  "mop-tok.prod.lb.js",
  "mop.uat.lb.js",
  "mop.sit.lb.js",
  "telecash.prod.lb.js",
  "telecash.uat.lb.js",
  "telecash.sit.lb.js",
  "wsv.prod.clientnet.lb.js",
  "wsv.prod.external.lb.js",
  "wsv.prod.internal.lb.js",
  "wsv.uat.lb.js",
  "wsv.sit.lb.js"
];

let nodes = [];

$(onLoad);

function onLoad() {
  $("#nav-placeholder").load("navbar.html");
  loadScripts();
}

function loadScripts(scriptIndex) {
  if (!scriptIndex) scriptIndex = 0; // assume first loading if not provided.
  if (!lbScripts[scriptIndex]) {
    initTable();
  } else {
    $.getScript("data/" + lbScripts[scriptIndex]).done(function () {

      var groupDesc = lbSetup.desc;
      $.each(lbSetup.nodes, function( index, value ) {
        value["group"] = groupDesc;
      });

      nodes = nodes.concat(lbSetup.nodes);
      loadScripts(scriptIndex + 1);
    });
  }
}

class TableItem {
  constructor() {
    this.group= "";
    this.url = "";
    this.ip = "";
    this.name = "";
    this.type = "";
    this.zone = "";
    this.comment = "";
    this.internal_nat = "";
    this.dr_internal_nat = "";
    this.deviceType = "";
  }

  setData(i) {
    if (i.group) { this.group = i.group; }
    if (i.url) { this.url = i.url.replace(",", "<br>"); }
    if (i.ip) { this.ip = i.ip; }
    if (i.name) { this.name = i.name; }
    if (i.type) { this.type = i.type; }
    if (i.zone) { this.zone = i.zone; }
    if (i.comment) { this.comment = i.comment; }
    if (i.internal_nat) { this.internal_nat = i.internal_nat; }
    if (i.dr_internal_nat) { this.dr_internal_nat = i.dr_internal_nat; }
    if (i.deviceType) { this.deviceType = i.deviceType; }
  }
}

class TopLevelItem extends TableItem {
  constructor(item) {
    super();
    this.setData(item);
  }
}

class ServerItem extends TableItem {
  constructor(item, parent) {
    super();
    this.setData(item);
    if (parent.group) this.group = parent.group;
    if (parent.zone) this.zone = parent.zone;
    if (this.type === "" && parent.type) this.type = parent.type;
  }
}

class LBItem extends TableItem {
  constructor(item, parent) {
    super();
    this.setData(item);
    if (parent.group) this.group = parent.group;
    if (parent.zone) this.zone = parent.zone;
    if (this.type === "" && parent.type) this.type = parent.type;
  }
}


function preprocessData() {
  let data = [];

  nodes.forEach(function (e) {

    data.push(new TopLevelItem(e));

    if (e.servers) {
      e.servers.forEach(function (s) {
        data.push(new ServerItem(s, e));
      })
    } else {
      if (e.primary) {
        data.push(new LBItem(e.primary, e));
      }
      if (e.dr) {
        data.push(new LBItem(e.dr, e));
      }
    }
  });
  return data;
}

function initTable() {

  let data = preprocessData();

  let table = $('#myTable').DataTable({
    data: data,
    paging: false,
    dom: 'ti',         // defines what to display - table + info
    ordering: false,
    fixedHeader: true,
    columns: [
      {data: "zone", title: "Zone"},
      {data: "url", title: "URL", width: "60px"},
      {data: "name", title: "Name"},
      {data: "ip", title: "IP"},
      {data: "internal_nat", title: "internal_nat"},
      {data: "dr_internal_nat", title: "dr_internal_nat"},
      {data: "type", title: "Type"},
      {data: "deviceType", title: "Device Type"},
      {data: "comment", title: "Comment"}
    ],
//            rowGroup: true,
    rowGroup: {
      dataSrc: 'group'
    },
    buttons: [
      {
        extend: 'copy',
        text: 'Copy All',
      },
      {
        extend: 'copy',
        text: 'Copy IPs',
        header: false,
        title: null,
        exportOptions: {
          columns: [3]
        }
      }, 'csv', 'excel',
//                {
//                    extend:'colvis',
//                    text: 'Columns ...'
//                }
    ]
  });

  table.buttons().container().appendTo($('#btns'));

  $('#filter-text-box').keyup(function () {
    table.search($(this).val()).draw();
  });

}
