
if (typeof jQuery === 'undefined') {
    console.error('Missing jQuery module.');
}

if (typeof login === "undefined") {
    console.error('Missing login module. Requires login.js');
}

var jira = {
    data: {}
}

jira.APIPath = '/rest/api/2/'; // used with node http-server - tests locally
var hname = window.location.hostname;
if (hname.includes('1dc.com')) {
    jira.APIPath = '/infra/jira-rest/'; // used with Apache where jira rest is mapped to this path
}
console.log('Jira API path: ' + jira.APIPath);

jira.ajaxSettings = {
    type : 'GET',
    xhrFields: {
        withCredentials: true
    },
    beforeSend: function (xhr) {
        xhr.setRequestHeader("Authorization", "Basic " + btoa(login.uname + ":" + login.psw));
    },
    error: function(data){
        console.error('Cannot load JIRA data');
        console.error(data);
    },
}

// Add methods, link to parent and children ...
jira.enhanceIssue = function(issue) {
    if (issue.parent) {
        // TODO link to the parent
    }

    // issue.open = function() {
    //     return 1;
    // } ;
    // item.closed = function() {
    //     return 1;
    // } ;

}

jira.loadData = function(url, callback) {
    var t = this;
    $.ajax(url, this.ajaxSettings)
        .done( function(data){
            if (data.issues) {
                console.log('JIRA issues loaded: ' + data.issues.length);
                data.issues.forEach(function (item) {
                    t.enhanceIssue(item);
                });
            } else {
                console.log('JIRA issues loaded: 1');
                // TODO handling of a single issue
            }

            if (typeof callback === "function") {
                callback(data);
            } else {
                console.error('Callback is not a function to call');
            }
        });
}

jira.loadFastTrackTasks = function(fastTrackId, callback) {
    this.loadData(this.APIPath + 'search?jql=project=TASK AND "Fast Track ID"=' + fastTrackId + ' AND type != "Sub Task"', function(data){
        // here goes custom code for this load type ....
        callback(data);
    });
}

jira.loadSubTasks = function(parentTaskId, callback) {
    this.loadData(this.APIPath + 'search?jql=project=TASK AND parent=' + parentTaskId, function(data){
        // here goes custom code for this load type ....
        callback(data);
    });
}

function issueLinkRenderer(params) {
    return '<a href="https://jirasdlc.1dc.com:8443/browse/'+params.value+'" target="_blank">Link</a>'
}

function statusRenderer(params) {
    var count = 0, closedCount = 0;
    params.node.childrenAfterGroup.forEach(function(child){
        count++;
        if (child.data.fields.status.name === 'Closed' ) {
            closedCount++;
        }
    });
    var result;
    if (params.value === 'Open' || params.value === 'Ready' || params.value === 'Re-Open') {
        result = '<span class="btn-xs btn-primary">' + params.value + '</span>';
    } else if (params.value === 'Hold' ) {
        result = '<span class="btn-xs btn-warning">' + params.value + '</span>';
    } else if (params.value === 'In Progress' ) {
        result = '<span class="btn-xs btn-info">' + params.value + '</span>';
    } else if (params.value === 'Acceptance' || params.value === 'Review') {
        result = '<span class="btn-xs btn-success">' + params.value + '</span>';
    } else if (params.value === 'Closed' ) {
        result = '<span class="btn-xs btn-success" style="background: darkgray">' + params.value + '</span>';
    } else {
        result = params.value;
    }
    if (count > 0) result = result + ' <small>(' + closedCount + ' of ' + count + ' issues closed.)</small>';
    return result;
}
