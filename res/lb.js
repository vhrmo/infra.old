// Create elements here
console.log("Creating custom elements.");

class JBossIcon extends HTMLElement {
    connectedCallback() {
        this.innerHTML = "<div class='icon'><span style=\"font-weight:bold;font-family:'Arial Black';font-size:larger\"><span style=\"color: red\">JB</span><span style=\"color: dimgray\">oss4</span></span></div>";
    }
}
customElements.define('ico-jboss4', JBossIcon);

class WflyIcon extends HTMLElement {
    connectedCallback() {
        this.innerHTML = "<div class='icon'><img src='res/wildfly-logo_small.png' width='150px' height='50px'></img></div>";
    }
}
customElements.define('ico-wildfly', WflyIcon);

class LBElement extends HTMLElement {
    // A getter/setter for a disabled property.
    get inited() {
        return this.hasAttribute('inited');
    }

    set inited(val) {
        // Reflect the value of the disabled property as an HTML attribute.
        if (val) {
            this.setAttribute('inited', '');
        } else {
            this.removeAttribute('inited');
        }
    }

    connectedCallback() {
        if (!this.inited) {
            this.inited = 'done';
            this.innerHTML = this.getHTML();
        }
    }
}

class Warning extends LBElement {
    getHTML() {
        return "<div class='warning-label'><i class='warning-icon fa fa-exclamation-triangle'></i>&nbsp;" + this.innerHTML + "</div>";
    }
}
customElements.define('x-warning', Warning);

class Error extends LBElement {
    getHTML() {
        return "<div class='error-label'><i class='error-icon fa fa-exclamation-circle' ></i>&nbsp;" + this.innerHTML + "</div>";
    }
}
customElements.define('x-error', Error);

class XUrl extends LBElement {
    // A getter/setter for a disabled property.
    get sslLabs() {
        return this.hasAttribute('sslLabs');
    }

    set sslLabs(val) {
        // Reflect the value of the disabled property as an HTML attribute.
        if (val) {
            this.setAttribute('sslLabs', '');
        } else {
            this.removeAttribute('sslLabs');
        }
    }

    getHTML() {
        var res =  "<a target='_blank' title='Open site in new tab' href='https://" + this.innerHTML + "'>" + this.innerHTML + "</a>";
        if (this.sslLabs) {
            res += "&nbsp;<a target='_blank' href='https://www.ssllabs.com/ssltest/analyze.html?d=" + this.innerHTML + "'>" +
                "<img title='Run SSL Labs scan' src=res/qualys-logo.png width='14px'></img></a>";
        }
        return res;
    }
}
customElements.define('x-url', XUrl);

class XIP extends LBElement {
    getHTML() {
        return  "<a target='_blank' title='Open site in new tab' href='https://" + this.innerHTML + "'>" + this.innerHTML + "</a>";
    }
}
customElements.define('x-ip', XIP);

class XContexts extends LBElement {
    getHTML() {
        var result = "<div class='contexts'><div class='contexts-title'>Contexts:</div><div class='contexts-links'>";
        var contexts = this.innerHTML.split(',');
        for (var i = 0; i < contexts.length; i++) {
            result += "<a target='_blank' title='Open in new tab' href='https://" +
                this.getAttribute('baseUrl') + "/" + contexts[i] + "'>/" + contexts[i] + "</a><br>";
        }
        result += "</div>";
        return result;
    }
}
customElements.define('x-contexts', XContexts);

class LBIcon extends LBElement {

    formatPopoverHtml(text) {
        try {
            let result = text;
            // this must be first replacement!
            result = result.replace(/(\"[^\"]*\")/g, '<span class="lb-info-text">$1</span>');
            // this one requires \n to be still in the text
            result = result.replace(/^(.*)$/m, '<span class="lb-info-heading">$1</span>');
            // now we can remove new lines
            result = result.replace(/\n/g, "<br>");
            result = result.replace(/([0-9]+[.,]?[0-9]*)/g, '<span class="lb-info-number">$1</span>');
            result = result.replace(/ ?(UP|ENABLED)/g, '<span class="lb-info-enabled"> $1</span>');
            result = result.replace(/ ?(DOWN|DISABLED)/g, '<span class="lb-info-disabled"> $1</span>');
            result += "<br><br>";
            return result;
        } catch (e) {
            return ""
        }
    }

    getHTML() {
        var vip = this.innerHTML;
        var popoverHtml = "No data available.";
        try {
            var vipInfo = appviewx_data[vip];
            popoverHtml = '<div class="lb-info">';
            popoverHtml += this.formatPopoverHtml(vipInfo['vip_tls_config']);
            popoverHtml += this.formatPopoverHtml(vipInfo['server_pool']);
            popoverHtml += this.formatPopoverHtml(vipInfo['monitor_setting']);
            // popoverHtml += this.formatPopoverHtml(vipInfo['full_vip_info']);
            popoverHtml += "</div>";
        } catch (e) {
            console.error("Problem with vip" + vip, e);
        }
        return "<div class='device-icon'>" +
            "<img src='res/load-balancer.svg' width='45' height='40' data-container='body' data-html='true' data-toggle='popover' data-trigger='hover' data-content='" + popoverHtml + "'></img>" +
            "</div>";
    }
}
customElements.define('x-load-balancer', LBIcon);

class Renderer {
    renderNode() {

    }
}

class LoadBalancerRenderer {
    formatPopoverHtml(text) {
        try {
            let result = text;
            // this must be first replacement!
            result = result.replace(/(\"[^\"]*\")/g, '<span class=lb-info-text>$1</span>');
            // this one requires \n to be still in the text
            result = result.replace(/^(.*)$/m, '<span class=lb-info-heading>$1</span>');
            // now we can remove new lines
            result = result.replace(/\n/g, "<br>");
            result = result.replace(/([0-9]+[.,]?[0-9]*)/g, '<span class=lb-info-number>$1</span>');
            result = result.replace(/ ?(UP|ENABLED)/g, '<span class=lb-info-enabled> $1</span>');
            result = result.replace(/ ?(DOWN|DISABLED)/g, '<span class=lb-info-disabled> $1</span>');
            result += "<br><br>";
            return result;
        } catch (e) {
            return ""
        }
    }

    renderPopover(e) {
        var vip = e.ip;
        var popoverHtml = "No data available.";
        try {
            var vipInfo = appviewx_data[vip];
            popoverHtml = '<div class=lb-info>';
            popoverHtml += this.formatPopoverHtml(vipInfo['vip_tls_config']);
            popoverHtml += this.formatPopoverHtml(vipInfo['server_pool']);
            popoverHtml += this.formatPopoverHtml(vipInfo['monitor_setting']);
            // popoverHtml += this.formatPopoverHtml(vipInfo['full_vip_info']);
            popoverHtml += "</div>";
        } catch (e) {
            console.error("Problem with vip" + vip, e);
        }
        console.log(popoverHtml);
        return popoverHtml;
    }
}

class WebServerRenderer extends Renderer {
    append(value, desc) {
        let v = "";
        if (value === "ENABLED") {
            v = "<span class=lb-info-enabled>ENABLED</span>"
        } else if (value === "DISABLED") {
            v = "<span class=lb-info-disabled>DISABLED</span>"
        }
        return desc + v + "<br>";
    }

    renderPopover(e) {
        if (ssl_scan_data[e.ip]) {
            let data = ssl_scan_data[e.ip];
            let result = "";
            result += this.append(data["-ssl2"],   "SSL2   : ");
            result += this.append(data["-ssl3"],   "SSL3   : ");
            result += this.append(data["-tls1"],   "TLS1.0 : ");
            result += this.append(data["-tls1_1"], "TLS1.1 : ");
            result += this.append(data["-tls1_2"], "TLS1.2 : ");
            return result;

        }
        return null;
    }
}

/*******************************************************************************
 * Initialize the diagram
 *******************************************************************************/

$(window).on('load', function(){
    $("#nav-placeholder").load("navbar.html");

    console.log("Initializing diagram. Mermaid version: "+mermaidAPI.version());
    var lb = GetURLParameter('lb');

    $.getScript("data/" + lb + ".js").done (function (script, textStatus) {
        var g = mermaidJSON.generateGraph(lbSetup, {
            renderers: {
                // list of renderers based on deviceType property
                'default': new Renderer(),
                'web-server': new WebServerRenderer(),
                // 'load-balancer': new LoadBalancerRenderer()
            }
        });
        // console.debug(g);
        document.getElementById("h1").innerHTML = lbSetup.desc;
        document.title = lbSetup.desc;

        var cb = function(svgGraph) {
            document.getElementById("id1").innerHTML = svgGraph;
            document.getElementById("loader").style.display = "none";
            // graph loaded - init popovers
            $('[data-toggle="popover"]').popover();
        };

        mermaidAPI.render('idx', g, cb, document.getElementById("id1"));
    });
});

