## BV Security Concept

### Definitions

#### Basic

- **IAM** – identity and access management team, owner of user management processes
- **WebLogin** - application with web interface where users can provide credentials in order to log in, and be redirected to their target application (SSO)
- **BV** (Benutzer- und Berechtigungsverwaltung) - application which provides IAM with web frontend to add/modify/delete users and their access permissions
- **BSServices** - application publishes webservice endpoints for applications which can query information about users and their access permissions or add/modify/delete users
- **Database** - database used by BV and BSServices to store metadata and information about access permissions
- **LDAP** - user directory, storage of user credentials and access control groups used by BV and BSServices and WebLogin

#### BV Specific

- **Metadata** - contains information about application, profile class, profile type, access method and category, this information is stored in database and does not change often and it can not be changed without developer interaction
- **Application** - application which depends on BV or BSServices when applying its own security, for example "BV", "EDP", "MOP"
- **Profile Class** - classifies profile type, there are 4 distinct profile classes CROSS, DIRECT, ORGA, CROSS SMALLGUI
- **Profile Type** - for specific application only profiles with predefined profile types can be created, profile type classifies profile, it is specific for application and profile class, for example "BV NODE DIRECT", "EDP CROSS" or "MOP CROSS"
- **Profile** - collection of permissions, each permission references specific object, profile can be assigned to an user
- **Access Method** - application can have multiple access methods and access method classifies permissions which can be created for a profile, example of access methods are "BV administrieren", "MOP use" or "edoks Dokumente lesen"
- **Category** - access method can have multiple categories which further classify what kind of object can permission reference, for example "APS PRODUCT", "BCS PRODUCT", "MOP PARTNER"
- **TPS** - database does not contain information about objects, it stores only references which are called extIds (external IDs), third party system, is implementation of an interface within BV, which reads object data from external system, may it be external database, LDAP or SAP, which TPS is used is determined by category and it is configurable in `BS/Konfiguration.xml`
- **Permission** - permission grants user access to perform certain action, it is classified by combination of profile and access method and references object
- **Object** - in context of this document object means authorization object, which is a role user can own or a resource that can be accessed only by users owning certain permissions, for example object can be LDAP group or LDAP node containing users
- **Group** - is a role stored in LDAP, however in context of BV, group is also an object, which can be referenced by permission, for example "bsProfileEditGrp", bsSuperAdminGrp", "bsUserUploadGrp"

### Profiles

##### Profile Classes

| Profile Class | Description
| --- | --- |
| **CROSS** | Cross profiles resolve permissions using additional object mappings. The cross product is made up of authorizations and objects. |
| **DIRECT** | Contains the concrete permissions. As for now only profile types for application BV use DIRECT profile class. |
| **ORGA** | Organization profiles do not need to be assigned to a user. Permissions are tied to the organization. As for now only profile types for application EDOKS use ORGA profile class. |
| **CROSS SMALLGUI** | Analogue cross profiles, however, with limited gui optimized for mass data. |

###### DIRECT vs. CROSS/CROSS SMALLGUI
Difference between DIRECT profile and CROSS profile (or ORGA profile for that matter) is that DIRECT profile contains permissions.
Object references within these permissions are referencing groups/roles so two different users assigned the same profile have exactly the same permissions.
**On the other hand CROSS profile may contain permissions, however when cross profile is assigned to the user authorization object must be specified and these permissions relate to this object.**
So two different users can have assigned the same CROSS profile but permissions to two different authorization objects.
For example these users can be allowed to read APS PARTNER, but it is a different one.

- [ ] **TODO: vhr: tomuto uplne nerozumiem**

###### CROSS/CROSS SMALLGUI vs ORGA
ORGA profile class works exactly as CROSS, the only difference is that authorization object is not tied to the user but to an organization.

- [ ] **TODO: vhr: Toto by chcelo lepsie vysvetlit. Viem si domysliet, ale hodil by sa popis a mozno aj diagram.** 

###### CROSS vs. CROSS SMALLGUI
The difference between CROSS and CROSS SMALLGUI is that CROSS provides user with GUI, where user can select objects from a list.
When assigning CROSS SMALLGUI objects user is provided with no choice and must enter external ID directly to an edit box.

- [ ] **TODO vhr: ako sa implementuje to CROSS GUI - je to custom pre kazde <nieco>???? Nejake voditko do kodu - implements interface alebo extends base class?**

##### Profile Types used in BV application

There are only 3 profile types which can be created for **BV** application and all have **DIRECT** profile class.

| Application | Profile class | Profile type | Access method | Category | Description |
| --- | --- | --- | --- | --- | --- |
| BV | DIRECT | BV NODE DIRECT | BV_USER_ADMIN | BV ORGANISATION | Create, change or delete user |
|  |  |  | BV_PROF_ADMIN | BV PROFILES | Create profile |
|  |  |  | BV_APP_USE | BV GROUP | Assign groups via application roles |
|  |  |  | BV_CP_ASSIGN | APS PARTNER | Objects that can be assigned to cross profiles loaded by TPS `ApsPartnerSystem` |
|  |  |  |  | MOP PARTNER | Objects that can be assigned to cross profiles loaded by TPS `MOPPartnerSystem` |
|  |  |  |  | ... | ... |
|  |  |  | BV_PROFILE_EDIT | BS APPLICATION | The profile owner can manage profiles for one or more applications |
|  |  | BV APP DIRECT | BV_APP_USE | BV GROUP | Assign groups via application roles |
|  |  | BS APP PROF DIRECT | BV_PROFILE_EDIT | BS APPLICATION | The profile owner can manage profiles for one or more applications |

###### Example

New administrator account is required, this administrator should be able to create, change or delete users in BV.
Let's assume user account for this administrator already exists, but does not have any profiles assigned.
Here is how to create new profile which can be later assigned to this administrator to provide him with permissions to create, change or delete users.

1. open BV, click profile>add
2. choose application _Benutzerverwaltung_ in combobox, once done profile type, access method and category is pre-populated (values are taken from metadata stored in database)
3. since profile type is pre-populated with _BV Knotenprofil_ (BV NODE DIRECT), access method with _BV administrieren_ (BV_USER_ADMIN) and category with _BV Organisation_ (BV ORGANISATION), which is exactly what is needed we just fill in name and description
4. validity dates are pre-populated by default with from current date to 31.12.9999 which serves as the end of time
5. category BV ORGANISATION is mapped to `de.gzs.bs.mapping.business.tps.impl.BVLdapNodeSystem`, this TPS returns all LDAP nodes representing organizations, these authorization objects are populated on UI in a form of list and can be added to the profile
6. once one or multiple organizations are chosen, profile can be saved and assigned to an administrator
7. administrator with this profile will be able to create, change or delete users in organisations (LDAP nodes) assigned to previously created profile (this functionality is implemented directly in BV, so BV must be aware what profile of type BV NODE DIRECT with access method BV_USER_ADMIN and category BV ORGANISATION means)

Another typical profile type would be BV APP DIRECT with BV_APP_USE access method (for example profile with name "MOP_ESP_full").
This profile type has category BV GROUP which maps to `de.gzs.bs.mapping.business.tps.impl.BVLdapGroupSystem` TPS which loads groups from LDAP.
Profile MOP_ESP_full contains 3 groups edpClientGrp, gzsESPMerchantGrp and mopPortalAccessGrp.
When user is assigned profile MOP_ESP_full then these three groups are automatically assigned to the user in LDAP.
Which means that the user will be able to access URLs which require roles edpClientGrp, gzsESPMerchantGrp and mopPortalAccessGrp in `web.xml`.
This is because WebLogin or more precisely custom LDAP login module in wildfly translates LDAP groups into application roles.    

### Groups

Groups can be created, changed or deleted in BV. Group can be assigned directly to the user or by means of a profile.
Some groups are assigned to the user automatically and some have special meaning in BV.  

#### Special groups

| Group name | Assignment | Description |
| --- | --- | --- |
| gzsInitialGrp, gzsInitialExternGrp, gzsESPInitialGrp | automatically when user is created | user account must be initialized first by changing password, then this group is removed and user account is usable |
| TechnicalUserGrp | by administrators in BV | group is used by special user accounts, used to access webservice endpoints by other applications |
| bsSuperAdminGrp | by administrators in BV | group causes some security checks to be skipped, especially when it is checked whether the user has access to a specific LDAP node to administer
| gzsDeniedGrp | by administrators in BV | group marks user account disabled |

- some groups are not stored in LDAP, but created by LoginModule and passed to WebLogin as roles. (AccountNotUsedGrp, gzsLoginAccountExpiredGrp, gzsLoginAccountNotValidGrp, gzsLoginDeniedGrp, gzsLoginDisabledGrp, gzsLoginPasswordExpiredGrp)

### Third Party System

All third party systems are classes in SecurityServices repository which implement `de.gzs.bs.factory.tps.ThirdPartySystem` interface.
Interface contains methods like `getObjects`, `getObjectsWithDescendants` or `getAllObjects`, these methods return data from external systems, typically from LDAP, different database, SAP system or SOAP webservice.
They may return all objects or only some, based on external IDs which are provided as parameters. Some TPS contain data structured to a tree in BV on UI there is combo box called reference type which can have values single or subtree. Methods which have word withDescendants in their name return subtree.
There is also custom cache implemented which stores results from TPS, this cache can be turned on or off by means of `BS/Konfiguration.xml` file.        

| Category | TPS Implementation | Description |
| --- | --- | --- |
| BCS MANDANT | `MandantSystem` |  |
| BCS PRODUCT | `EDPSystem` |  |
| MOP PARTNER | `MOPPartnerSystem` |  |
| TELECASH PRODUCT | `EDPSystem` |  |
| FALCO LOCATION | `FalcoLocationSystem` |  |
| TELECASH MANDANT | `TelecashSystem` |  |
| BV GROUP | `BVLdapGroupSystem` | Returns groups from LDAP (objectclass=gzsAccessGroup) |
| BV PROFILES | `BVProfileSystem` |  |
| BV ORGANISATION | `BVLdapNodeSystem` | Returns organizations from LDAP (objectclass=gzsOrganizationalUnit) |
| BS APPLICATION | `BSApplicationSystem` |  |
| EDOKS ORGANISATION | `EdoksOrganisationSystem` |  |
| EDOKS DOCUMENTCLASS | `EdoksDocClassSystem` |  |
| EDOKS JOBCLASS | `EdoksJobClassSystem` |  |
| EDOKS PERMISSION | `EdoksPermissionSystem` |  |
| APS PARTNER | `ApsPartnerSystem` |  |
| APS PRODUCT | `EDPSystem` |  |
| TCP PARTNER | `TCPartnerSystem` |  |
| GLOBALOBJ | `GlobalDummyObjectSystem` |  |
| TICKETSYSTEM ROLE | `TicketsystemRoleSystem` |  |
| IPG STORE | `IpgStoreSystem` |  |
| CONTROL TENANT | `TelecashTenantSystem` |  |
| PREDIGY TENANT | `PredigyTenantSystem` |  |        
| APS VIEW | `ApsHierarchieViewSystem` |  |
| INSIGHT FOLDER | `InsightFolderSystem` |  |
| INSIGHT ORGA | `InsightObjectSystem` |  |
| KUFO KNBISO | `KnbIsoSystem` |  |
| CC PREFIX | `PolControlPrefixSystem` |  |
| CC BRANCH | `PolControlTenantSystem` |  |
| COGNOS FOLDER | `CognosFolderSystem` |  |
| ACC FOLDER | `CognosAccFolderSystem` |  |
| ACC CUSTOMER | `AccCustomerSystem` |  |
| ACC BILLTO | `AccBillToSystem` |  |
| ACC BROKER | `AccBrokerSystem` |  |
| ACC HAENDLER | `AccHaendlerSystem` |  |
| SEPA CREDITOR | `SepaCreditorSystem` |  |
| EPS BRIDGE INST | `EpsBridgeSystem` |  |

### BSServices

BSServices is application which publishes webservice endpoints for other application in order to query/add/change/delete users and their permissions.
These are the same users and permissions which are administered using BV.
Other applications use this user information and permissions obtained from BSServices to apply their own programmatic security constraints.
These applications are fully responsible for enforcing permissions retrieved by webservices.

| Service | Usage | Description |
| --- | --- | --- |
| BridgeBSConnector | used | get user information and BRIDGE permissions |
| CognosBSConnector | used | get user information and COGNOS permissions |
| EdoksBSConnector |  |  |
| EdoksBVConnector |  |  |
| EdpBSConnector | used | get/add/delete EDP user permissions |
| EspBSConnector | used | get ESP user permissions |
| ~~EwasBSConnector~~ | unused |  |
| ~~FalcoBSConnector~~ | decommissioned |  |
| IpgBSConnector |  |  |
| KufoBSConnector | used | get user information and KUFO permissions |
| MopAddUserBSConnector |  |  |
| MopBSConnector | used | get user information and MOP permissions |
| PredigyBSConnector | used | get user information and PREDIGY permissions / change user password |
| RiskshieldBSConnector |  |  |
| SepaBSConnector | used | get user information and SEPA permissions |
| SoftpayBSConnector |  |  |
| TCControllingAddUserBSConnector |  |  |
| TCControllingComponentBSConnector | used | get user information and TCCONTROLLING permissions |
| TCPortalBSConnector |  |  |
| TicketsystemBSConnector |  |  |

### Database

Database is used by both BV and BSServices. It stores metadata and access permissions.
Metadata is data which determines what kind of profile can be created in BV and it changes only rarely.
It consists of Application, Profile Class, Profile Type, Access Method and Category.
Metadata can be changed only by developers by editing configuration file and then administrators can use BV to refresh it.
Access permissions are stored in Profile and Permission, these can be created, changed or deleted directly in BV.
Object references are also stored in database, these references are called External IDs and are used by TPS when authorization object is queried.


### LDAP

- Users are represented by leaf nodes which have object class `gzsPerson`. User has attributes like uid, givenName, mail...
- Groups are represented by leaf nodes which have object class `gzsAccessGroup`. Group has attribute members, which is a list of all users assigned to the group.
- Organizations are represented by nodes which have object class `organizationalUnit` and attribute `standardattributes` contains `isUserNode=true`. Organization nodes contain user nodes.

