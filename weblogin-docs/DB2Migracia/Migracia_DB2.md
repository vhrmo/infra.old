#### Analyza migracie BS DB2 databazy - [Jira Task](https://jirasdlc.1dc.com/browse/TASK-25219)

##### Pouzitie databazy

- JNDI meno dadasource `java:/db2bs`
- konfiguracia je v `BS/Konfiguration.xml/jndi` a pouzita v **BSCommmon** `de.gzs.bs.configuration.BSConfiguration`
- instancie su medzi prostrediami rozlisene pomocou schemy `*-WSV.properties` v premennej `xml.bs.table.schema`
- datasource je pouzity v repozitari **SecurityServices**
    - BSCommon (toto pouzitie sa da odstranit, nie je to bussines logika, iba infrastrukturny kod v `BaseServer.checkDS`)
    - BSEnterpriseEJB
- ziadna aplikacia nepouziva databazu priamo okrem tychto troch
- **TODO: overit, ci sa DB nepouziva v nejakych hookoch alebo ThirdPartySystemoch (TPS)**

| Aplikacia | Popis |
| --- | --- |
| BSServices | pristupuje do DB a cez webservices vracia vysledky tretim stranam |
| EDP | mame plne pod kontrolou my |
| UserAdmin | mame plne pod kontrolou my, sluzi iba na administraciu uzivatelov a ich prav |

| DAO Interface | DAO Implementation | Stored procedures descriptor |
| --- | --- | --- |
| `LoggerDAO` | `DB2LoggerDAO` | `storedprocedure.Authorisation.xml` |
| `MappingDAO` | `DB2MappingDAO` | `storedprocedure.Organisation.xml` |
| `MandantDAO` | `DB2MandantDAO` | `storedprocedure.Organisation.xml` |
| `AccessMethodDAO` | `DB2AccessMethodDAO` | `storedprocedure.Authorisation.xml` |
| `AuthorisationDAO` | `DB2AuthorisationDAO` | `storedprocedure.Authorisation.xml` |
| `AssigningDAO` | `DB2AssigningDAO` | `storedprocedure.Authorisation.xml` |
| `PartnerDAO` | `DB2PartnerDAO` | `storedprocedure.Organisation.xml` |
| `ClientViewDAO` | `DB2ClientViewDAO` | does not use stored procedures reads SQL from `BS/Konfiguration.xml` |

| Tabulka |  Popis |
| --- | --- |
| `SESSION` | Temporarna tabulka na predavanie parametrov pre storovane procedury |
| `T1090_PTN` | APS Partner table. Used in `DB2PartnerDAO.getPartnerWithDescendants`. |
| `T1091_PTN_HIER` | APS Partner table used with `T1090_PTN` to store partner hierarchy. |
| `T1489_AUTH_OBJECT` | Obsahuje externe kluce objektov, co su referencie na predmet profilu. V UserAdmin je mozne priradovat objekty profilu, alebo priamo konkretnemu uzivatelovi. V BSServices sa potom externe systemy dopytuju, ake objekty ma dany uzivatel priradene. Stlpec `OBJECT_ID` nema ziadny foregn key, ale napriek tomu je to referencia na `T1501_USR_PROF_XRF.OBJECT_ID` a `T1498_PERMISSION.OBJECT_ID` |
| `T1490_OBJECT_TYP` | Ciselnik typov objektov, kde `OBJECT_TYPE_CODE` je zadefinovane v `BS/Konfiguration.xml` ako mapovanie na Third Party System. |
| `T1491_KEY_LAYOUT` | |
| `T1492_BCS_MSTR_DAT` | V tabulke su ulozeni mandanti. Pouzite v `DB2MandantDAO.getMandants`. |
| `T1495_ACCESS_METH` | Ciselnik pristupovych metod, kde `NAME` moze byt 'BV administrieren' a `MATCH_CODE` napr. 'BV_USER_ADMIN' |
| `T1496_APPLICATION` | Ciselnik applikacii, kde `APPLICATION_CODE` nadobuda hodnoty ako 'BV' alebo 'EDP'|
| `T1497_ACC_OBJ_XREF` | Na user interface je to prelozene ako 'Category' a blizsie specifikuje access method, tj. jedna access methoda moze mat viacero kategorii. |
| `T1498_PERMISSION` | Permission je specificke pre profil a authorization method a obsahuje `OBJECT_ID` co je referencia do tabulky objektov `T1489_AUTH_OBJECT.OBJECT_ID`. Tymto sposobom su pre dany profil zadefinovane objekty, na ktore ma uzivatel pravo ak mu je pridany dany profil. |
| `T1499_PROFILE` | Profil obsahuje prava na objekty a je mozne ho priradit uzivatelovi. |
| `T1501_USR_PROF_XRF` | Priradenie profilu uzivatelovi cez `LDAP_RFRN`, co je user id ulozene v LDAP-e. `OBJECT_ID` je referencia na `T1489_AUTH_OBJECT.OBJECT_ID`, pomocou ktorej sa da priradit uzivatelovi konkretny objekt. |
| `T1502_LOGGING` | Loguju sa tu akcie ako napr. pridanie profilu. |
| `T1503_LOG_VALUES` | Loguju sa tu konkretne hodonty stlpcov pre zalogovane akcie. |
| `T1545_PROF_ACC_XRF` | Prepojovacia tabulka medzi `T1546_PROFILE_TYP` a `T1495_ACCESS_METH` |
| `T1546_PROFILE_TYP` | Ciselnik Profile Typ definuje cez `T1547_PROF_OBJ_XRF` ake typy objektu sa daju priradit profilu a cez `T1545_PROF_ACC_XRF`  |
| `T1547_PROF_OBJ_XRF` | Prepojovacia tabulka medzi `T1546_PROFILE_TYP` a `T1490_OBJECT_TYP` cez `OBJECT_TYP_CODE`, len tam nie je referencna integrita. |
| `T1548_PROF_CLASS` | Ciselnik Profile Class definuje akym sposobom sa zadava profil cez GUI, moze mat hodnoty CROSS, DIRECT, ORGA a CROSS SMALLGUI. |
| `T1549_KEY_TYP` | lokalizacia - ciselnik |
| `T1550_TEXT` | lokalizacia - ciselnik |
| `T1565_TC_MSTR_DAT` |  |
| `T1569_OBJ_PROF_XRF` | Priradenie objektov k organizacnym profilom. (Pouzite v `ProfileHelper.addObjectToProfile` typ profilu `ORGA`.) |
| `T1582_ONLINE_VIEW` | `getAPSClientView` select v `BS/Konfiguration.xml` |
| `T1583_KEY_TYP` | `getAPSClientView` select v `BS/Konfiguration.xml` |
| `T1584_VIEW_ATTR` | `getAPSClientView` select v `BS/Konfiguration.xml` |

![DB2 BS](DB2_BS.png)

###### UserAdmin UI
![Profil pridanie](1_add_profile.png)
![Profil zmena](2_edit_profile.png)
![Profil priradenie_uzivatela_profilu](3_assign_user.png)
![Profil priradenie_profilu_uzivatelovi](4_assign_profile.png)
![Uzivatel zmena_objektov_cross](5_select_user_object_assignments.png)
![Uzivatel zmena_objektov_cross](6_edit_user_object_assignments.png)
![Uzivatel zmena_objektov_cross_smallgui](7_edit_user_object_assignments_simple.png)
![Uzivatel zmena_objektov_orga](7_edit_user_object_assignments_orga.png)

###### Objekty autorizacie (UAT)

```
select ot.APPLICATION_CODE, ot.OBJECT_TYP_CODE, count(*) c
from T1489_AUTH_OBJECT ao
         join T1490_OBJECT_TYP ot on ao.OBJECT_TYP_ID = ot.OBJECT_TYP_ID
group by ot.APPLICATION_CODE, ot.OBJECT_TYP_CODE
order by c desc;
```

| APPLICATION_CODE | OBJECT_TYP_CODE | Pocet |
| --- | --- | --- |
| CONTROLLINGCOMPONENT	| CONTROL TENANT	| 40382 |
| EDOKS	| EDOKS DOCUMENTCLASS	| 3737 |
| EDP	| BCS MANDANT	| 2032 |
| ... | | |
| EDOKS	| EDOKS JOBCLASS	| 136 |
| BV	| BV ORGANISATION	| 95 |
| KUFO	| KUFO KNBISO	| 90 |
| ... | | |


###### Authorisation storovane procedury

| Procedura | Zlozitost* | Pouzitie | Popis |
| --- | --- | --- | --- |
| `DELETEOBJECTPROFIL` | 2 | `DB2AssigningDAO.removeObjectProfiles` | Deletuje zaznamy z tabulky `T1569_OBJ_PROF_XRF` podla `PROFILE_ID` a `OBJECT_ID`. |
| `DELETEPERMISSION` | 2 | `DB2AssigningDAO.removePermissions` | Deletuje zaznamy z tabulky `T1498_PERMISSION` podla `PROFILE_ID`, `ACCESS_METHOD_ID` a `OBJECT_ID` |
| `DELETEPROFILE` | 1 | `DB2AssigningDAO.deleteProfile` | Zmaze jeden zaznam z tabulky `T1499_PROFILE` podla `PROFILE_ID` |
| `DELETEUSERPROFILE` | 4 | `DB2AssigningDAO.deleteUserProfiles`, `DB2AssigningDAO.deleteUserProfilesByUserIdsAndProfilesIds` | Zmaze jeden alebo N zaznamov z tabulky `T1501_USR_PROF_XRF` podla `USER_PROFILE_ID` alebo podla `LDAP_RFRN` a `PROFILE_ID`. Prepisat na 2 alebo 3 metody. Vracia to data zo zmazanych zaznamov koli logovaniu do databazy. |
| `GETACCESSMETHODS` | 3 | `DB2AccessMethodDAO.getMetaData` | Selectne zaznamy do 6 tich kurzorov, treba prepisat na 6 metod. |
| `GETOPBYOID` | 1 | `DB2AssigningDAO.getObjectProfilesByObjectId` | Selectne zaznamy z tabulky `T1569_OBJ_PROF_XRF` podla `OBJECT_ID`. |
| `GETPERMISSION` | 2 | 2x`DB2AuthorisationDAO.getPermissions` | Selectne 4 kurzory. Prepisat na 4 metody. |
| `GETPROFILE` | 3 | `DB2AssigningDAO.getAllProfiles`, 2x`DB2AssigningDAO.getProfiles`, `DB2AssigningDAO.getProfileByProfileName` | Obsahuje 4 selecty do tabulky `T1499_PROFILE`, prepisat na 4 metody. |
| `GETUPBYOID` | 1 | `DB2AssigningDAO.getUserProfilesByObjectId` | Selectne zaznamy z tabulky `T1501_USR_PROF_XRF` podla `OBJECT_ID` |
| `GETUSERPROFILE` | 3 | `DB2AssigningDAO.getAllUserProfiles`, `DB2AssigningDAO.getUserProfiles`, `DB2AssigningDAO.getUserProfilesByProfileIds`, `DB2AssigningDAO.getUserProfilesByUserIdsAndProfileIds`, `DB2AssigningDAO.getUserProfilesByUserIds` | Obsahuje 5 roznych selectov nad tabulkou `T1501_USR_PROF_XRF`, prepisat na 5 metod. |
| `INSERTLOGGING` | 3 | `DB2LoggerDAO.insert` | Insertuje zaznam do tabulky `T1502_LOGGING`. Obsahuje obdobny error handling pri vracani vygenerovanej IDENTITY hodnoty, ako ostatne procedury. Tieto chyby sa stavaju pri paralelnom uploade uzivatelov. |
| `INSERTLOGVALUES` | 2 | `DB2LoggerDAO.insert` | Insertuje zaznam do tabulky `T1503_LOG_VALUES`, napr. ak sa vytvori profil, tu budu ulozene samotne hodnoty toho profilu, ktory bol vytvoreny. |
| `INSERTOBJECTPROFIL` | 2 | `DB2AssigningDAO.insertObjectProfiles` | Insertuje zaznamy do tabulky `T1569_OBJ_PROF_XRF`. |
| `INSERTPERMISSION` | 2 | `DB2AssigningDAO.insertPermissions` | Insertuje zaznamy do tabulky `T1498_PERMISSION`. |
| `INSERTPROFILE` | 1 | `DB2AssigningDAO.insertProfile` | Insertuje zaznamy do tabulky `T1499_PROFILE`. |
| `INSERTUSERPROFILE` | 3 | `DB2AssigningDAO.insertUserProfiles` | Insertuje zaznamy do tabulky `T1501_USR_PROF_XRF`. |
| `SETUPTEMP` | 0 | - | Iba vytvara temporarne tabulky, cez ktore sa predavaju parametre storovanym proceduram. Nie je potrebne migrovat. |
| `UPDATEPROFILE` | 1 | `DB2AssigningDAO.updateProfile` | Updatne hodnoty pre konretny profil v tabulke `T1499_PROFILE`. |

###### Organisation storovane procedury

| Procedura | Zlozitost* | Pouzitie | Popis |
| --- | --- | --- | --- |
| `DELETEMAPPINGS` | 1 | `DB2MappingDAO.deleteUniqueKeys` | Deletuje zaznamy z tabulky `T1489_AUTH_OBJECT` podla `OBJECT_ID`. |
| `GETAPSPTNWITHDESC` | 8 | `DB2PartnerDAO.getPartner`, `DB2PartnerDAO.getPartnerWithDescendants` | Selectuje data z tabuliek `T1090_PTN` a `T1091_PTN_HIER`, kde su ulozene hierarchicke data. |
| `GETMANDANT` | 3 | `DB2MandantDAO.getMandants`, `DB2MandantDAO.getAllMandants` | Selectuje zaznamy z tabulky `T1492_BCS_MSTR_DAT`, vsetky alebo podla `EXT_KEY`. Prepisat na 3 metody. |
| `GETMAPPINGS` | 3 | 3x`DB2MappingDAO.getMappings`  | Selectuje data z tabuliek `T1489_AUTH_OBJECT` a `T1490_OBJECT_TYP` podla roznych kriterii. Obsahuje 3 selecty. |
| `GETPERMISSION` | 4 | ? | Selectuje data z tabuliek `T1498_PERMISSION`, `T1495_ACCESS_METH`, `T1499_PROFILE` a `T1569_OBJ_PROF_XRF`, obsahuje 4 rozne selecty. |
| `NEWMAPPINGS` | 4 | `DB2MappingDAO.getNewUniqueKey`, `DB2MappingDAO.getNewUniqueKeys`, `DB2MappingDAO.getNewKey` | Insertuje zaznamy do tabulky `T1489_AUTH_OBJECT` podla roznych kriterii. |

- **Zlozitost*** - zlozitost prepisania procedury pomocou JPA od 1 najjednoduchsie do 10 najzlozitejsie


##### Aktualny stav

- business logika je implementovana storovanymi procedurami, ktore su pre nas **black box** a nedaju sa jednoducho debugovat
- custom framework na volanie storovanych procedur a DAO-cka si sami vytvaraju programaticky JDBC konekcie a pouzivaju custom `JDBCTransactionContext`, preto sa nedaju DAO-cka lahko prepisat aby pouzivali CDI, preto sa ani nedaju lahko prepisat EJB-cka z 2.x na 3
- framework na volanie storovanych procedur si vytvara temporarne tabulky pomocou, ktorych si predava parametre do storovanych procedur, pretoze procedury mozu mat variabilny pocet parametrov
- VO (value object) obsahuju business logiku, ktora nie je thread safe `de.gzs.common.util.VO`
- `de.gzs.bs.authorisation.business.logger.LoggerThread` toto je zle, vlastne thready by sa nemali vytvarat takymto sposobom, navyse ta synchronizacia v `de.gzs.bs.authorisation.business.logger.Logger` moze sposobit, ze vela threadov caka len na to aby mohli zapisat log
- pri uploade pouzivatelov v BSServices sa vola funkcionalita na pridavanie profilov, ktora niekedy nefunguje ked sa importuje viac uzivatelov naraz, zrejme tam tiez nieco nieje thread safe, mozu to byt aj storovane procedury
- DAO/Repository triedy by mali byt stateless, vacsina stavu z DAO tried bola vyextrahovana do `CacheSingleton`, ale napr. toto tu este ostalo `de.gzs.bs.authorisation.dao.db2.DB2AccessMethodDAO.accessMethods` i ked sa to uz plni z toho singletonu...
- `de.gzs.bs.factory.tps.ThirdPartySystem` tu sa vytvaraju instancie tried cez reflexiu a este navyse aj obsahuju stav, reflexia by sa nemala pouzivat vobec
- `de.gzs.bs.mapping.business.tps.impl` tu su naimplementovane vsetky tie TPS (Third Party System), ktore sa vytvaraju cez reflexiu

##### Ziadany stav

###### UserAdmin (BV)
- pouzivaju sa EJB3, nieje potrebne generovat nic cez XDoclet ani menit ejb-jar.xml

| EJB2 | EJB3 |
| --- | --- |
| AuthorisationBO | ostava, premenuje sa na AuthorisationBean |
| AuthorisationBean | X |
| AuthorisationServer | X |
| AuthorisationBD | X |
| AuthorisationHome | X |
| AuthorisationLocal | ostava, momentalne sa pouzivaju Remote interface, ale pouzivat Local by bolo lepsie |
| AuthorisationLocalHome | X |
| AuthorisationRemote | X |
| AuthorisationUtil | X |

'X' -> zmaze sa

- vsetky datasourcy sa injectuju cez anotacie a nevytvaraju sa programaticky, pripadne vobec pouziva sa priamo EntityManager
- pouziva sa JPA, entity su namapovane priamo na tabulky a views
- read-only pristup do databazy bude vracat DTO-cka, ktore nebudu obsahovat ziadnu business logiku
- mohlo by sa odstranit BS/Konfiguration.xml
- existuju aspon nejake integracne testy, ktore by pracovali priamo s databazou
- nepouziva sa reflexia, vsetky instancie sa vytvaraju cez CDI
- nevytvaraju sa nove Thread-y cez new Thread...moze sa pouzit anotacia @Asynchronous, ked to ma zmysel
- nemusime riesit DB schemu, bolo by dobre spytat sa DB2 ludi, ci nam mozu vytvorit synonyma
- audit do databazovych tabuliek sa robi dokladne a automaticky vid. [Envers](http://docs.jboss.org/hibernate/orm/current/userguide/html_single/Hibernate_User_Guide.html#envers)
- vsetky dependencies, ktore ohodnoti SNYK ako nebezpecne, budu odstranene

###### BSServices

- uplne odstranit AXIS specificku funkcionalitu ak to bude mozne pouzit cisto iba JAX-WS (mozno je tam nejaky WS co pouziva WS-security)
- vsetky dependencies, ktore ohodnoti SNYK ako nebezpecne budu odstranene
- nebudu sa na backende pouzivat ziadne triedy zo servlet specifikacie ako napr. HttpServletRequest a podobne...

##### Moznosti migracie

- spytat sa DB supportu, ci nam vedia vytvorit synonyma, aby sme sa vyhli pouzitiu schemy v nasej konfiguracii/kode...

##### Ine

- ked sa v BV vytvori nova grupa, tak sa okrem zaznamu v LDAP-e vytvori aj zaznam v tabulke `T1489_AUTH_OBJECT`
- ked priradim grupu priamo uzivatelovi, ako sa to prejavi v DB a bude to tam vobec, ci sa to zapise len do LDAP-u?
- kde su skutocne ulozene mena tych objektov, ktore su v tabulke `T1489_AUTH_OBJECT`? Ziskavaju sa cez `ThirdPartySystem` TPS implementacie? 
- BSServices pouzivaju T1495_ACCESS_METH.MATCH_CODE ked sa dopytuju na permissions
- vazba PROFILE_TYP -> APPLICATION -> ACCESS_METH je pouzita, ked sa vytvara profil a uzivatel si vybera v kombo boxe access method
- Profily su pouzite na formularoch pre vytvaranie uzivatelov, ktore vyplnaju klienti (formularom sa mysli excel/pdf-ko)
- Pouzivane BSServices: [Confluence](https://escmconfluence.1dc.com/display/BBW/Permission+service+webservice)
- bolo by mozne cele riesenie nahradit standardom? napr. OAuth2?
- niektore volania v MappingBO, ktore vracaju objekty resp. ich kluce su cachovane, vid. `BSConfiguration.CACHE_ACTIVATED`

| Trieda profilu | Popis
| --- | --- |
| **CROSS** | Cross profiles resolve permissions using additional object mappings. The cross product is made up of authorizations and objects. |
| **DIRECT** | Contains the concrete permissions. |
| **ORGA** | Organization profiles do not need to be assigned to a user. Permissions are tied to the organization. |
| **CROSS SMALLGUI** | Analogue cross profiles, however, with limited gui optimized for mass data. |
 
- Application BV profiles overview SQL script
```
select ap.APPLICATION_CODE, pc.PROFILE_CLASS_CODE, pt.PROFILE_TYP_CODE, am.MATCH_CODE, aox.OBJECT_TYP_CODE, am.DESCRIPTION
from T1546_PROFILE_TYP pt
         join T1548_PROF_CLASS pc on pt.PROFILE_CLASS_ID = pc.PROFILE_CLASS_ID
         join T1496_APPLICATION ap on pt.APPLICATION_ID = ap.APPLICATION_ID
         join T1545_PROF_ACC_XRF pax on pt.PROFILE_TYP_ID = pax.PROFILE_TYP_ID
         join T1495_ACCESS_METH am on pax.ACCESS_METHOD_ID = am.ACCESS_METHOD_ID
         join T1497_ACC_OBJ_XREF aox on am.ACCESS_METHOD_ID = aox.ACCESS_METHOD_ID
where ap.APPLICATION_CODE = 'BV';
```
