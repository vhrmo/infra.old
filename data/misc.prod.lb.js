var lbSetup = {
    desc: "MISC PROD web app access setup",

    nodes: [

        // ===========================================================
        // PROD servers
        {
            url: "misc.firstdata.eu",
            zone: "Internet",
            type: 'jboss4',
            contexts: [
                'login',
                'mandat',
                // 'merchant-migration/app',
                'CurrencyCalculator/fremdwaehrungskurse',
                'CurrencyCalculator/hvbkurse'
            ],
            primary: { url: 'misc-pr.firstdata.eu', ip: '217.73.36.83', internal_nat: '10.74.66.73'},
            dr: { url: 'misc-dr.firstdata.eu', ip: '217.73.38.83', internal_nat: '10.72.66.73'}
        },
        {
            name: "External LB",
            ip: "10.74.66.73",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                { name: 'n4pvwb1006-vipa02', ip: '10.74.74.52', url: 'misc.firstdata.eu', deviceType: "web-server"},
                { name: 'n4pvwb1007-vipa02', ip: '10.74.74.54', url: 'misc.firstdata.eu', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB DR",
            ip: "10.72.66.73",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                { name: 'n5pvwb1006-vipa02', ip: '10.72.74.52', url: 'misc.firstdata.eu', deviceType: "web-server"},
                { name: 'n5pvwb1007-vipa02', ip: '10.72.74.54', url: 'misc.firstdata.eu', deviceType: "web-server"}
            ]
        },
        {
            url: "misc.firstdata.eu",
            zone: "FD Net",
            type: 'jboss4',
            contexts: [
                'login',
                'mandat',
                // 'merchant-migration/app',
                'CurrencyCalculator/fremdwaehrungskurse',
                'CurrencyCalculator/hvbkurse'
            ],
            primary: { url: 'misc-pr.firstdata.eu', ip: '10.74.66.73'},
            dr: { url: 'misc-dr.firstdata.eu', ip: '10.72.66.73'}
        }


    ]};
