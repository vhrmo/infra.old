var lbSetup = {
    desc: "MISC UAT web app access setup",

    nodes: [

        // ===========================================================
        // UAT servers
        {
            url: "misc-u-i.firstdata.eu,misc-u.firstdata.eu",
            zone: "Internet",
            type: 'jboss4',
            error: 'Not accessible from Internet',
            contexts: [
                'login',
                'mandat',
                // 'merchant-migration/app',
                'CurrencyCalculator/fremdwaehrungskurse',
                'CurrencyCalculator/hvbkurse'
            ],
            primary: { url: 'misc-u-i-pr.firstdata.eu', ip: '217.73.36.93', internal_nat: '10.74.66.78'},
            dr: { url: 'misc-u-i-dr.firstdata.eu', ip: '217.73.38.93', internal_nat: '10.72.66.78'}
        },
        {
            name: "External LB",
            ip: "10.74.66.78",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                { name: 'n4cvwb1006-vipa02', ip: '10.74.74.48', url: 'misc-u.firstdata.eu', deviceType: "web-server"},
                { name: 'n4cvwb1007-vipa02', ip: '10.74.74.50', url: 'misc-u.firstdata.eu', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB DR",
            ip: "10.72.66.78",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                { name: 'n5cvwb1002-vipa02', ip: '10.72.74.48', url: 'misc-u.firstdata.eu', deviceType: "web-server"},
                { name: 'n5cvwb1003-vipa02', ip: '10.72.74.50', url: 'misc-u.firstdata.eu', deviceType: "web-server"}
            ]
        },
        {
            url: "misc-u-i.firstdata.eu,misc-u.firstdata.eu",
            zone: "FD Net",
            type: 'jboss4',
            contexts: [
                'login',
                'mandat',
                // 'merchant-migration/app',
                'CurrencyCalculator/fremdwaehrungskurse',
                'CurrencyCalculator/hvbkurse'
            ],
            primary: { url: 'misc-u-i-pr.firstdata.eu', ip: '10.74.66.78'},
            dr: { url: 'misc-u-i-dr.firstdata.eu', ip: '10.72.66.78'}
        },


    ]};
