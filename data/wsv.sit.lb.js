var lbSetup = {
    desc: "WSV SIT and DEV web app access setup",

    nodes: [
        // ===========================================================
        // DEV servers
        {
            url: "online-dev.firstdata.de",
            zone: "FD Net",
            type: "jboss4",
            comment: "Hosts legacy JBoss4 apps until they are migrated to Wildfly.",
            primary: { url: "online-dev.firstdata.de", ip: "10.77.17.168", type: "jboss4" }
        },
        { url: "online-dev.firstdata.de", name: "n5dvwb995-vipa04", zone: "Application non-restricted zone - A2-NR", ip: "10.77.17.168", type: "jboss4", deviceType: "web-server"},
        {
            url: "online-dev.1dc.com",
            zone: "FD Net",
            type: "wildfly",
            comment: "Wildfly apps.",
            primary: { url: "online-dev.1dc.com", ip: "10.77.17.163", type: "wildfly"}
        },
        { name: 'n5dvwb995-vipa02', ip: '10.77.17.163', url: 'online-dev.1dc.com', zone: "Application non-restricted zone - A2-NR", type: 'wildfly', deviceType: "web-server"},

        // ===========================================================
        // APS URLs
        {
            url: "aps-i.any.firstdata.de",
            zone: "FD Net",
            primary: { url: "aps-i.any.firstdata.de", ip: "tbd1" }
        },
        {
            url: "aps-i.demo.firstdata.de",
            zone: "FD Net",
            primary: { url: "aps-i.demo.firstdata.de", ip: "tbd2" }
        },
        {
            url: "aps-i.ges1.firstdata.de",
            zone: "FD Net",
            primary: { url: "aps-i.ges1.firstdata.de", ip: "tbd3" }
        },
        {
            url: "aps-i.ges2.firstdata.de",
            zone: "FD Net",
            primary: { url: "aps-i.ges2.firstdata.de", ip: "tbd4" }
        },


        // ===========================================================
        // SIT servers
        {
            url: "online-i.firstdata.de",
            zone: "FD Net",
            type: "jboss4",
            comment: "Hosts legacy JBoss4 apps until they are migrated to Wildfly.",
            primary: { url: "online-i.firstdata.de", ip: "10.77.17.167" ,type: "jboss4", error: 'TLS1 enabled!'}
        },
        {name: "n5dvwb995-vipa03", url: "online-i.firstdata.de", zone: "Application non-restricted zone - A2-NR", ip: "10.77.17.167" ,type: "jboss4", deviceType: "web-server"},
        {
            url: "online-i.1dc.com",
            zone: "FD Net",
            type: "wildfly",
            comment: "Wildfly apps.",
            contexts: [
                'login',
                'BSServices',
                'amos',
                'awis_edoks',
                'awissb',
                'bv',
                'class',
                'edp',
                'esp',
                'espservice',
                'gans',
                'gans-l',
                'gans-t',
                'gans-o',
                'gans-q',
                'gans-h'
            ],
            primary: { url: "online-i.1dc.com", ip: "10.77.17.162", type: "wildfly"}
        },
        { name: 'n5dvwb995-vipa01', ip: '10.77.17.162', url: 'online-i.1dc.com', zone: "Application non-restricted zone - A2-NR", type: "wildfly", deviceType: "web-server"},


    ]};
