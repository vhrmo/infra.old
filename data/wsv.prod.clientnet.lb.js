var lbSetup = {
    desc: "WSV PROD ClientNet web app access setup",

    classes: [
        {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [
        // ===========================================================
        // Concardis and CardProcess link - online.firstdata.de - needed for DWH online
        {
            name: 'prod-wsv-legacy-int-online',
            url: "online.firstdata.de",
            ip: "170.186.103.243",
            zone: "Client Net",
            contexts: [
                'login',
                'dwhmis/distribution',
                'edp'
            ],
            warning: 'Fiducia proxy config changes are needed to make this available VR payment (formerly CardProcess)',
            comment: "Concardis and VR payment (formerly CardProcess) connectivity via B2B<br><br>" +
                "RHI used to failover to DR in case service is not available <br>" +
                "on primary side. RHI switches NAT for the client net IP <br>" +
                "to the DR load balancer IP instead of primary one.",
            primary: {
                name: "ONLINE.FIRSTDATA.DE",
                ip: "10.82.250.32",
                internal_nat: "10.68.196.41x",
                comment: "ClientNet LB VIP"
            },
            dr: {
                name: "ONLINE.FIRSTDATA.DE",
                ip: "10.83.250.32",
                internal_nat: "10.77.196.41x",
                comment: "ClientNet LB DR VIP"
            }
        },
        {
            url: "online.firstdata.de",
            ip: "10.68.196.41x",
            type: "jboss4",
            zone: "Application restricted zone - A2-R",
            class: 'duplicate',
        },
        {
            url: "online.firstdata.de DR",
            ip: "10.77.196.41x",
            type: "jboss4",
            zone: "Application restricted zone - A2-R",
            class: 'duplicate',
        },


        // ===========================================================
        // Concardis and CardProcess link - online.1dc.com
        {
            zone: "Fiducia connected clients",
            url: "online.firstdata.de",
            ip: "217.73.36.47x",
            comment: "Fiducia source NAT: 10.86.120.25/32<br><br>" +
                'Fiducia proxy translates public online.firstdata.de <br>' +
                'IP address to FD internal IP address space forcing their users <br>' +
                'to reach FD internal servers via FD Client Net.<br><br>' +
                'Clients connected by Fiducia include VR payment (formerly CardProcess), <br>' +
                'Issuer banks - Merkur bank etc. ',
            link: { ip: "170.186.103.18" }
        },
        {
            zone: "Concardis Eschborn",
            name: "Source NAT: 10.86.80.129/32",
            // url: "online.firstdata.de",
            comment: "Concardis Eschborn source NAT: 10.86.80.129/32",
            link: { ip: "170.186.103.18" }
        },
        {
            name: 'prod-wsv-int-online',
            url: "online.firstdata.de",
            ip: "170.186.103.18",
            zone: "Client Net",
            comment: "Concardis and VR payment (formerly CardProcess) connectivity via B2B<br><br>" +
                "RHI used to failover to DR in case service is not available <br>" +
                "on primary side. RHI switches NAT for the client net IP <br>" +
                "to the DR load balancer IP instead of primary one.",
            warning: "URL to be changed to online.1dc.com to match the servers it is reaching.",
            contexts: [
                'login',
                'amos',
                'esp',
                'edp',
                'gans'
            ],
            primary: {
                name: "ONLINE-ESH-170-186-103-18-VIP",
                ip: "10.82.250.30",
                internal_nat: "10.68.196.12x",
                comment: "ClientNet LB VIP"
            },
            dr: {
                name: "ONLINE-EQU-170-186-105-18-VIP",
                ip: "10.83.250.30",
                internal_nat: "10.77.196.17x",
                comment: "ClientNet LB DR VIP"
            }
        },
        {
            url: "online.1dc.com",
            ip: "10.68.196.12x",
            type: "wildfly",
            zone: "Application restricted zone - A2-R",
            class: 'duplicate',
        },
        {
            url: "online.1dc.com DR",
            ip: "10.77.196.17x",
            type: "wildfly",
            zone: "Application restricted zone - A2-R",
            class: 'duplicate',
        },

        // ===========================================================
        // VISECA link
        {
            zone: "VISECA",
            url: 'online.vis.firstdata.de',
            comment: "VISECA source NAT: 10.86.67.192/26",
            link: {
                ip: "170.186.103.119"
            }
        },
        {
            zone: "Client Net",
            url: "online.vis.firstdata.de",
            type: 'wildfly',
            ip: "170.186.103.119",
            // internal_nat: "10.68.196.45",
            // dr_internal_nat: "10.77.196.45",
            contexts: [
                'login',
                'gans',
            ],
            comment: "VISECA link to the WSV applications.<br><br>" +
                "RHI used to failover to DR in case service is not available <br>" +
                "on primary side. RHI switches NAT for the client net IP <br>" +
                "to the DR load balancer IP instead of primary one.",
            primary: {
                name: "ONLINE-FIRSTDATA-DE-RHI-VIP",
                ip: "10.82.250.23",
                internal_nat: "10.68.196.45",
                comment: "ClientNet LB VIP"
            },
            dr: {
                name: "ONLINE-FIRSTDATA-DE-RHI-VIP",
                ip: "10.83.250.23",
                internal_nat: "10.77.196.45",
                comment: "ClientNet LB DR VIP"
            }
        },
        {
            url: "online.vis.firstdata.de",
            name: "VIS Internal LB",
            zone: "Application restricted zone - A2-R",
            ip: "10.68.196.45",
            type: 'wildfly',
            deviceType: 'load-balancer',
            comment: 'LB re-built to target Wildfly - #9265',
            servers: [
                {name: "n4pvwb011-vipa02", ip: "10.68.5.251", url: "online.vis.firstdata.de", comment: 'Apache 2.2 - update to 2.4 and Wildfly', deviceType: "web-server", class: 'modify'},
                {name: "n4pvwb012-vipa02", ip: "10.68.6.0", url: "online.vis.firstdata.de", comment: 'Apache 2.2 - update to 2.4 and Wildfly', deviceType: "web-server", class: 'modify'}
            ]
        },
        {
            url: "online.vis.firstdata.de",
            name: "VIS Internal LB DR",
            ip: "10.77.196.45",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            class: "wildfly",
            comment: "LB setup rebuilt to standard - AppViewX #1617.",
            warning: "Note: the IP of the online.vis.firstdata.de doesn't automatically<br>" +
                "change to DR IP within FDNet since firstdata.de domain <br>" +
                "doesn't support it. It'll work in ClientNet though <br>" +
                "because RHI will automatically change the NAT.",
            servers: [
                {name: "n5bvwb992-vipa02", ip: "10.77.5.193", url: "online.vis.firstdata.de", comment: 'Apache 2.4 ready', deviceType: "web-server"},
                {name: "n5bvwb993-vipa02", ip: "10.77.5.188", url: "online.vis.firstdata.de", comment: 'Apache 2.4 ready', deviceType: "web-server"}
            ]
        },


    ]};
