var lbSetup = {
    desc: "MOP & Tokenizer UAT web app access setup",


    nodes: [

        // ===========================================================
        // Future external access
        {
            zone: "Internet",
            url: 'mopq.firstdata.eu',
            type: 'wildfly',
            contexts: [
                'mop/fdi',
                'mop/tc',
                'mop/aibms',
                'mop/fdms',
                'mop/fda',
            ],
            primary: { url: 'esh-mopq-new.firstdata.eu', ip: '217.73.36.48', internal_nat: '10.74.66.8'},
            dr: { url: 'eqx-mopq-new.firstdata.eu', ip: '217.73.38.11', internal_nat: '10.72.66.12'}
        },
        {
            name: "External LB",
            ip: "10.74.66.8",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'r4cvwb1011-vipa01', ip: '10.74.68.104', url: 'mopq.firstdata.eu', type:'wildfly', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB DR",
            ip: "10.72.66.12",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'r5cvwb1010-vipa01', ip: '10.72.68.109', url: 'mopq.firstdata.eu', type:'wildfly', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Internal access
        {
            zone: "FD Net",
            url: 'mopq.firstdata.eu,mopq-new.firstdata.eu',
            type: 'wildfly',
            contexts: [
                'mop/fdi',
                'mop/tc',
                'mop/aibms',
                'mop/fdms',
                'mop/fda',
            ],
            primary: { url: 'esh-mopq-new.firstdata.eu', ip: '10.74.66.8'},
            dr: { url: 'eqx-mopq-new.firstdata.eu', ip: '10.72.66.12'}
        },


        // ===========================================================
        // Tokenizer Internal access
        {
            zone: "FD Net",
            url: 'tokenq.1dc.com',
            type: 'wildfly',
            primary: { url: 'esh-tokenq-new.1dc.com', ip: '10.68.196.8'},
            dr: { url: 'eqx-tokenq-new.1dc.com', ip: '10.77.196.13'}
        },
        {
            name: "Internal LB",
            ip: "10.68.196.8",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Application  restricted zone - A2-R",
            servers: [
                { name: 'r4cvwb1026-vipa01', ip: '10.68.7.57', url: 'tokenq.1dc.com', deviceType: "web-server"}
            ]
        },
        {
            name: "Internal LB DR",
            ip: "10.77.196.13",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Application  restricted zone - A2-R",
            servers: [
                { name: 'r5cvwb1019-vipa01', ip: '10.77.5.54', url: 'tokenq.1dc.com', deviceType: "web-server"}
            ]
        },
        {
            zone: "FD Net",
            url: 'tokenq.1dc.com',
            class: 'unused',
            dr: { url: 'tokenq2.1dc.com', ip: '10.77.49.105', class: 'unused', warning: 'To be removed - REQ000002733754'}
        },
        { name: 'a5qvwb999-vipaY', ip: '10.77.49.105', url: 'tokenq.1dc.com', class: 'unused', warning: 'to be decommissioned', deviceType: "web-server"},

        // ===========================================================
        // Tokenizer client net access
        // {
        //     zone: "Client Net",
        //     url: 'tokenq.1dc.com',
            // primary: { url: 'token1.1dc.com', ip: '170.186.103.144', internal_nat:'10.68.37.113'},
            // dr: { url: 'token2.1dc.com', ip: '170.186.105.88', internal_nat:'10.77.37.113'}
        // },

    ]
};
