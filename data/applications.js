var Applications =

    [
        {cluster: 'IAM & SSO', UAID: 'UAID - 03939', Name: 'AppGuard',Description:'For NGPOS applications such as Pogo and Clover a new single sign on solution named AppGuard (based on OpenAM) was introduced to provide a log-in solution for end users with federation capabilities. AppGuard is derived from Forgerock\'s OpenAM (see http://forgerock.com/products/open-identity-stack/openam) and contains customizations required for the usage for First Data.<br>' +
                '<br>' +
                'The main idea is that users only have to log in once and automatically are authenticated for all end user pages that a user requires in the context of NGPOS.<br>' +
                '<br>' +
                'These end user pages are spread over several applications which partially are hosted on different platforms.'},
        {cluster: 'IAM & SSO', UAID: 'UAID - E0167', Name: 'Weblogin', Description:'Weblogin is a SSO solution for German web applications. Provides authentication and authorization service to all aplications which are integrated with Weblogin. After login it presents to the user the starting page with links to all applications a user has access to.<br>' +
                '<br>' +
                'Both internal and external users are using the service. User data are validated against LDAP.'},

        {cluster: 'WSV', UAID: 'UAID-E0168', Name: 'Benutzerverwaltung (BV)', Description:'User Administration System for applications supported by Weblogin. Configuration of users, usergroups, clients, access rights etc. The application manages central user profiles in LDAP and stores additional information in a relational database. The system provides a flexible profile and role concept. Apart from functional permissions (access roles for a specific application) the system also supports management of data permissions (permissions on entities in the target applications).'},
        {cluster: 'WSV', UAID: 'UAID-E0075', Name: 'AWIS', Description:'D secure service registration with the card schemes (MasterCard/VISA) via Arcot/Netcetera.<br>' +
                '<br>' +
                '3-D Secure is an XML-based protocol designed to be an additional security layer for online credit and debit card transactions. It was originally developed by Arcot Systems, Inc and first deployed by VISA with the intention of improving the security of Internet payments. It is offered to customers as Verified by Visa and likewise MasterCard adopted this service as MasterCard SecureCode.<br>' +
                '<br>' +
                'Similar service is provided by http://www.netcetera.com/en/products/3dsecure.html.'},
        {cluster: 'WSV', UAID: 'UAID-E0065', Name: 'AMOS', Description:'AMOS (Acquiring Merchant Online System) is used by FD call centers and acquirers (ConCardis, VR payment (formerly CardProcess), FDMS, AIBMS, ICP). Provides access to transaction information and merchant business volume data.<br>' +
                '<br>' +
                '·         Supports Suspense and Reject Handling.<br>' +
                '<br>' +
                '·         Supports automate letter generation/batch mailings.<br>' +
                '<br>' +
                '·         Supports Manual Transaction Adjustments e.g. Manual fees (Terminal rental fee, Statement copy fee, Minimum monthly charge, Terminal rental fee).<br>' +
                '<br>' +
                '·         All master data are provided from ACOM (Merchant boarding system).<br>' +
                '<br>' +
                'Top clients: FDMS, ConCardis, AIBMS (UK), DZ Bank, VR payment (formerly CardProcess)'},
        {cluster: 'WSV', UAID: 'UAID-E0078', Name: 'CLASS-Online', Description:'Class Online is web a front-end for CLASS (Clearing And Settlement System) on the German Mainframe. CLASS is a clearing Gateway to all international card schemes for all EMAXS acquirers; Clearing Gateway from MasterCard for all ADVANTAGE issuers; processing platform for all German CrossBorder Debit Transactions (Acquiring ATM, Issuing ATM and POS)<br>' +
                '<br>' +
                'It has only few internal FD users.'},
        {cluster: 'WSV', UAID: 'UAID-E0077', Name: 'GANS-Online', Description:'Gans online is  a web front end for GANS (Global Authorization Network System). App users manage card/cardholder data and authorize/force/decline transactions. It is used by many acquiring and issuing users, both internal (call center in Nurnberg and Bad Vilbel) and external via Citrix (Viseca, BayernCard, Communigate, VR payment (formerly CardProcess), Aduno, ValovisBank, HSH Nordbank, National Bank, ...)<br>' +
                '<br>' +
                'Acquiring functionality<br>' +
                '<br>' +
                '·         Merchant- / Terminal parameter<br>' +
                '<br>' +
                '·         Terminal- / contract info, Contract data<br>' +
                '<br>' +
                '·         Cash- / Retail info, Acceptance type<br>' +
                '<br>' +
                '·         MCC (merchant category code)<br>' +
                '<br>' +
                '·         Merchant- / Terminal data<br>' +
                '<br>' +
                '·         Merchant Authorization<br>' +
                '<br>' +
                'Issuing functionality<br>' +
                '<br>' +
                '·         Customer and cardholder data <br>' +
                '<br>' +
                '·         Credit limits on card and at customer level rsp. related available to buy<br>' +
                '<br>' +
                '·         Security values (e.g. PIN-trial counter)<br>' +
                '<br>' +
                '·         Status on Card and at customer level,<br>' +
                '<br>' +
                '·         Authorization Details (Display, Cancellation, Generation, Enforcement'},
        {cluster: 'WSV', UAID: 'UAID-E0067', Name: 'EDP', Description:'EDP (Electronic Document Presentment). Provides access to edoks documents via the web interface. Report, and other documents can be downloaded. Also used for Telecash Voucher Archive (IKEA) + other clients (FDMS, ConCardis, AIBMS, Viseca)<br>' +
                '<br>' +
                'Own? web application and as in iframe within other apps like MOP.'},
        {cluster: 'WSV', UAID: 'UAID-E0066', Name: 'ESP', Description:'ESP (Electronic Statement Presentation) is a web application, which provides users access to electronic statements in different formats like pdf, csv, xml and html. Documents are generated on fly based on input data and implemented templates.<br>' +
                '<br>' +
                'Application is used by FD call centers and customers (ConCardis, VR payment (formerly CardProcess), FDMS, AIBMS, ICP). '},
        {cluster: 'WSV', UAID: 'UAID-04428', Name: 'GTR Online', Description:'GTR is a transaction history database. While authorization systems keep transaction data only for limited amount of time in their database to ensure fast data access during authorization processing, transactions are submitted to GTR database for data warehousing reasons.<br>' +
                '<br>' +
                'GTR online application provides basic functionality to run a search in GTR database and browse / download the search results. Queries are run in the background, so user can initiate multiple queries and they are executed based on server load in the background.'},
        {cluster: 'WSV', UAID: 'UAID-04025', Name: 'SEPA Payment monitoring', Description:'Web application for monitoring of SEPA transaction processing in all involved systems – APS, ADV, CLASS, Cashway, XmlConverter, File transfer. Used by GCC to identify the root cause of an issue.'},

        {cluster: 'MISC', UAID: 'UAID - 03113', Name: 'SEPA Mandate management',Description:'For certain types of SEPA payments merchants are required to sign a contract with the customer. Such a contract is called a mandate which must be registered via this app. Payments are validated against these mandates and declined if not available.'},
        {cluster: 'MISC', UAID: 'UAID - 03116', Name: 'CurrencyCalculator',Description:'Currency calculator is a web screen to display historical exchange rates.<br>' +
                '<br>' +
                'Production URL: https://misc.firstdata.eu/CurrencyCalculator/fremdwaehrungskurse/<br>' +
                '<br>' +
                'Production URL UniCredit (not used anymore): https://misc.firstdata.eu/CurrencyCalculator/hvbkurse/<br>' +
                '<br>' +
                'Workflow:<br>' +
                '<br>' +
                '·         SAP generates exchange rate files on daily basis (ZKD files)<br>' +
                '<br>' +
                '·         CurCalc java spring batch import job (FwhImportJob) parses the files and inserts into database (job is scheduled by cron)<br>' +
                '<br>' +
                '·         Web application (ExchangeRatesWeb) only shows read-only view of the exchange rates'},


        {cluster: 'MOP', UAID: 'UAID - 02590', Name: 'MOP',Description:'MOP (Merchant Online Portal) - module based information portal which provides transaction processing and billing details for our Acquirers and merchants. It includes:<br>' +
                '<br>' +
                '·         Reporting (ESP, EDP), <br>' +
                '<br>' +
                '·         MOP - Research <br>' +
                '<br>' +
                '·         MOP - Chargeback Inbox (TicketSystem). Interactive Chargeback and Retrieval Request correspondence.<br>' +
                '<br>' +
                '·         Flexible Report creation and Report Group/Scheduler functionality<br>' +
                '<br>' +
                '·         Supports acquirer specific look & feel of the Web UI, which makes it look differently for each acquirer. <br>' +
                '<br>' +
                'Top clients: FDMS and AIBMS'},
        {cluster: 'MOP', UAID: 'UAID - E0198', Name: 'Tokenizer', Description:'\t<br>' +
                'PCI data tokenization service. A service used by SAP RIS, ADV, APS, CBK, FALCON, GTR. Application to replace credit card data by tokens applicable for several use cases.<br>' +
                '<br>' +
                'Application consists of multiple modules:<br>' +
                '<br>' +
                '§  batch module for inbound / outbound PCI data masking / unmasking<br>' +
                '<br>' +
                '§  web service module - provides applications an option to mask / unmask PAN data with legacy Ingrian based algorithm, not used for Voltage based masking<br>' +
                '<br>' +
                '§  web application for users which need to be able to mask / unmask PAN data for a valid business reasons (reporting to schemes, chargeback process etc)<br>' +
                '<br>' +
                'A Voltage algorithm support has been implemented to Tokenizer to provide dependent applications ability to migrate to new FD standard for masking sensitive data.'},


        {cluster: 'Batch', UAID: 'UAID -E0072', Name: 'FD Batch'},
        {cluster: 'Batch', UAID: 'UAID -04091', Name: 'XML Converter',Description:'The XML Converter is a batch job for creating SEPA XML files from ADV database tables. It covers:<br>' +
                '<br>' +
                '·         acquiring transactions processing (payments to merchant accounts)<br>' +
                '<br>' +
                '·         issuing transactions processing <br>' +
                '<br>' +
                'The traffic can be split to:<br>' +
                '<br>' +
                '·         outbound XML processing - payments being sent to SEPA EBICS (http://www.ebics.org)<br>' +
                '<br>' +
                '·         inbound XML processing - confirmations for payments sent to SEPA'},


        {cluster: 'docFlow', UAID: 'UAID -E0191', Name: 'docFlow',Description:'Visa\\MC chargeback documentation processing and archiving.<br>' +
                '<br>' +
                'Application supporting chargeback process with VISA and MasterCard. Application supports exchange of additional documentation between issuer and acquirer during the chargeback process.'},

        {cluster: 'Telecash', UAID: 'UAID -E0104', Name: 'Kufo Web',Description:'Kundenfocus: Tool for Customers of Telecash (KNB Kaufmännische Netzbetreiber / ISO Independent Sales Organization). Enables them to setup tarifs for their customers (Merchants) and do billing incl. invoice generation.<br>' +
                '<br>' +
                'KufoWEB is a billing application which is used by KNB-ISOs. KNB-ISOs like Sparkassen and Volksbanken act as reseller for TeleCash Terminals. Whenever a retailer wants to have a Terminal, the retailer and KNB-ISO conclude an agreement about the terminal and the used Tariff. In the background a BSA between the KNB-ISO and Telecash is concluded. During the duration of the agreement each month TeleCash creates an invoice for the Terminal. <br>' +
                '<br>' +
                'Each month this TeleCash invoice data and the corresponding master data (like BSA and Terminal data) is loaded to KufoWEB data base. The KNBISO uses the KufoWEB web application to maintain tariffs and important data for the billing like the business accounts of its retailers. After the data is loaded, the KNB-ISO can generate and download the retailer invoices and payment files (DTA/SEPA) for this month. Additionally KufoWEB provides a report area to generate and download useful reports.'},
        {cluster: 'Telecash', UAID: 'UAID -E0049', Name: 'Pol Control',Description:'The purpose of this application is ad-hoc reporting for employees of Polizei Niedersachsen to check transactions (fines) paid to police over Point of Sale terminal.'},


        {cluster: 'Unnamed', UAID: 'UAID -02455', Name: 'DWH -Online',Description:'Viseca Data warehouse application. DWH online is a reporting application used to generate Reports (ad-hoc/scheduled) and a platform to deliver generated reports via different transport channels to customers. It contains a fine grained permission model for the generation and delivery of the reports.<br>' +
                '<br>'},
        {cluster: 'Unnamed', UAID: 'UAID -E0076', Name: 'WebADV',Description:'WebADV is a web based application which provides access the ADV DB2 database. It provides read access to the ADV change history.'},

    ];

var content = "";
let printedClusters = [];


Applications.forEach(generateRow);
$('#tablebody').append(content);






function testfn(index, tdElement) {



    if (tdElement.parentNode.hasAttribute("open")) {
        tdElement.parentNode.nextSibling.remove();
        tdElement.parentNode.removeAttribute("open");



    }else {




        descriptioRow = document.createElement("tr");
        descriptioRow.innerHTML = "<td colspan='3' class='Description'>" +  Applications[index].Description + "</td>";
        insertAfter(descriptioRow, tdElement.parentElement);
        tdElement.parentNode.setAttribute("open", "1");




    }

}



function generateRow(item, index, arrays) {

    var columns = "";
    var clusterhead = "";

    if (!printedClusters.includes(item.cluster)) {
        clusterhead = columns + "<th colspan='3'>" + item.cluster + "</th>";
        printedClusters.push(item.cluster);
    }

    columns = columns + "<td onclick='testfn(\"" + index + "\", this)'>" + item.UAID + "</td>";
    columns = columns + "<td>" + item.Name + "</td>";
    columns = columns + "<td>" + "<a href='https://echo.1dc.com/appmap/" + item.UAID + "/ApplicationDetails/'>appmap</a>" + "</td>";

    content = content + "<tr>" + clusterhead + "</tr>" + "<tr>" + columns + "</tr>";



}
var test = "<tr><td>fsdf</td></tr>";

document.getElementById("tablebody").innerHTML = content;


function insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);


}




