var lbSetup = {
    desc: "DWH online PROD web app access setup",

    classes: [
        {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [
        // ===========================================================
        // New external URL to be used for Wildfly
        {
            url: "dwhonline.firstdata.eu",
            zone: "Internet",
            contexts: [
                'login',
                'dwhmis/distribution'
            ],
            comment: 'LB request 2637',
            primary: {url: "esh-dwhonline.firstdata.eu", ip: "217.73.36.138", internal_nat: "10.74.66.59", warning: 'Tufin 73180'},
            dr: {url: "eqx-dwhonline.firstdata.eu", ip: "217.73.38.24", internal_nat: "10.72.66.54", warning: 'Tufin 73180'}
        },
        {
            url: "esh-dwhonline.firstdata.eu",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            ip:  "10.74.66.59",
            servers: [
                { name: 'n4pvwb1006-vipa03', ip: '10.74.74.53', url: 'dwhonline.firstdata.eu', warning: 'apache setup needed', deviceType: "web-server"},
                { name: 'n4pvwb1007-vipa03', ip: '10.74.74.55', url: 'dwhonline.firstdata.eu', warning: 'apache setup needed', deviceType: "web-server"},
            ]
        },
        {
            url: "eqx-dwhonline.firstdata.eu",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            ip: "10.72.66.54",
            servers: [
                { name: 'n5pvwb1006-vipa03', ip: '10.72.74.53', url: 'dwhonline.firstdata.eu', warning: 'apache setup needed', deviceType: "web-server"},
                { name: 'n5pvwb1007-vipa03', ip: '10.72.74.55', url: 'dwhonline.firstdata.eu', warning: 'apache setup needed', deviceType: "web-server"},
            ]
        },

    ]};
