var lbSetup = {
    desc: "WSV UAT web app access setup",

    nodes: [
        // ===========================================================
        // Internet connectivity
        {
            url: "online-qa.firstdata.com",
            zone: "Internet",
            type: "wildfly",
            class: 'modify',
            comment: "To be used to host new Wildfly apps. <br> Only migrated apps will be available externally.",
            primary: {url: "online-qa.firstdata.com", ip: "217.73.38.174", internal_nat: "10.72.66.31", type:"wildfly", class: 'modify'},
        },
        {
            name: "External LB",
            ip: "10.72.66.31",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            class: 'modify',
            servers: [
                { name: "n5cvwb995-vipa01", ip: "10.72.68.103", url: "online-qa.firstdata.de", type: "wildfly", class: "modify", deviceType: "web-server"},
                { name: "r5cvwb1003-vipa01", ip: "10.72.68.102", url: "online-qa.firstdata.de", type: "wildfly", class: "modify", deviceType: "web-server"}
            ]
        },
        {
            url: "bi.uat.firstdata.com",
            zone: "Internet",
            comment: "Cognos URL. Cognos is using WSV web servers <br> which forward the traffic to Cognos Windows based app servers.",
            primary: {url: "bi.uat.firstdata.com", ip: "217.73.38.59", internal_nat: "10.72.66.56"}
        },
        {
            name: "Cognos External LB",
            ip: "10.72.66.56",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                { name: "n5cvwb995-vipa02", ip: "10.72.68.111", url: "bi.uat.firstdata.com", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Client Net connectivity
        {
            url: "online-qa.firstdata.de",
            zone: "Client Net",
            primary: {
                url: "online-qa.firstdata.de",
                ip: "170.186.105.8",
                internal_nat: "10.2.30.182",
                class: "error",
                comment: "Incorrectly NAT-ed to old decommissioned server based on IPAM"
            }
        },
        {
            url: "online-qa.vis.firstdata.de",
            ip: "xx.xx.xx.aa",
            zone: "Client Net",
            class: "error"
        },
        {
            name: "Decommissioned old server",
            ip: "10.2.30.182",
            class: "error"
        },

        // ===========================================================
        // New URL to be used for Wildfly
        {
            url: "online-qa.1dc.com",
            zone: "FD Net",
            type: "wildfly",
            comment: "To be used to host new Wildfly apps.",
            contexts: [
                'login',
                'amos',
                'awis_edoks',
                'awissb',
                'bv',
                'class',
                'edp',
                'esp',
                'espservice',
                'gans'
            ],
            primary: {
                url: "online-qa.1dc.com",
                type: "wildfly",
                ip: "10.77.196.124"
            }
        },
        {
            name: "LB: ONLINE-QA-1DC-COM-VIP",
            ip:"10.77.196.124",
            zone: "Application restricted zone - A2-R",
            type: "wildfly",
            deviceType: 'load-balancer',
            servers: [
                { name: "n5cvwb993-vipa03", ip: "10.77.6.79", url: "online-qa.1dc.com", warning:"Health check disabled, TLS1 enabled,", type: "jboss4", deviceType: "web-server"},
                { name: "r5cvwb1005-vipa03", ip: "10.77.6.78", url: "online-qa.1dc.com", warning:"Wildfly with TLS1 enabled for PolControl", type: "wildfly", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Old URL for JBoss4
        {
            url: "online-qa.firstdata.de",
            zone: "FD Net",
            type: "jboss4",
            comment: 'To be used to host old JBoss4 apps until decommissioned. ',
            contexts: [
                'login',
                'awis_edoks',
                'awissb',
                'ews',
                'webADV',
                'dwhmis'
            ],
            primary: {
                url: "online-qa.firstdata.de",
                ip: "10.77.196.42"
            }
        },
        {
            name: "Internal LB",
            ip: "10.77.196.42",
            zone: "Application restricted zone - A2-R",
            type:"jboss4",
            deviceType: 'load-balancer',
            servers: [
                { name: "n5cvwb993-vipa01", ip: "10.77.5.187", url: "online-qa.firstdata.de", deviceType: "web-server", comment: "Used by Cognos and Kufo by directly accesing vipa", warning :"TLS1 enabled, healthcheck disabled"},
                { name: "r5cvwb1005-vipa01", ip: "10.77.5.186", url: "online-qa.firstdata.de", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // BS Services
        {
            url: "bs-qa.firstdata.de",
            zone: "FD Net",
            // ip: "10.77.6.81",
            type: "wildfly",
            contexts: ["BSServices"],
            primary: {
                url: "bs-qa.firstdata.de",
                ip: "10.77.196.125",
                type: "wildfly"
            }
        },
        {
            name: "Internal LB BSServices",
            ip:"10.77.196.125",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                { name: "n5cvwb993-vipa04", ip: "10.77.6.80", url: "bs-qa.firstdata.de", type: 'wildfly', deviceType: "web-server"},
                { name: "r5cvwb1005-vipa04", ip: "10.77.6.81", url: "bs-qa.firstdata.de", type: 'wildfly', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // BS Services
        {
            url: "bs-qa.1dc.com",
            zone: "FD Net",
            type: "wildfly",
            contexts: ["BSServices"],
            class: 'modify',
            primary: {
                url: "bs-qa.1dc.com",
                ip: "10.77.196.53",
                class: 'modify',
                warning: 'LB request 1374'
            }
        },
        {
            name: "Internal LB BSServices",
            ip: "10.77.196.53",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            class: 'modify',
            warning: 'LB request 1374',
            servers: [
                { name: "n5cvwb993-vipa04", ip: "10.77.6.80", url: "bs-qa.firstdata.de", type: 'wildfly', deviceType: "web-server"},
                { name: "r5cvwb1005-vipa04", ip: "10.77.6.81", url: "bs-qa.firstdata.de", type: 'wildfly', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Cognos
        {
            url: "bi.uat.1dc.com",
            zone: "FD Net",
            primary: {
                url: "bi.uat.1dc.com",
                ip: "10.77.196.46"
            }
        },
        {
            name: "Cognos internal LB",
            ip: "10.77.196.46",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                { name: "n5cvwb993-vipa02", ip: "10.77.5.220", url: "bi.uat.firstdata.com", deviceType: "web-server"},
                { name: "r5cvwb1005-vipa02", ip: "10.77.5.219", url: "bi.uat.firstdata.com", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // BCS
        {
            name: "Internal LB BCS",
            ip:"xx.xx.xx.yy",
            zone: "Application restricted zone - A2-R",
            class: "warning",
            servers: [
                { name: "n5cvwb993", ip: "10.77.5.155", url: "online-qa.bcs.firstdata.de", error: "To be removed from /etc/hosts", deviceType: "web-server"},
                { name: "r5cvwb1005", ip: "10.77.5.154", url: "online-qa.bcs.firstdata.de", error: "To be removed from /etc/hosts", deviceType: "web-server"}
            ]
        }

    ]};
