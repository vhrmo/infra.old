var lbSetup = {
    desc: "Tokenizer PROD web app access setup",

    classes: [
        // {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [

        // ===========================================================
        // Tokenizer Internal access
        {
            zone: "FD Net",
            url: 'token.1dc.com',
            type: 'wildfly',
            primary: { url: 'token-pr.1dc.com', ip: '10.68.196.79'},
            dr: { url: 'token-dr.1dc.com', ip: '10.77.196.73'}
        },
        {
            name: "Internal LB",
            ip: "10.68.196.79",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Application  restricted zone - A2-R",
            servers: [
                { name: 'r4pvwb1047-vipa02', ip: '10.68.5.106', url: 'token.1dc.com', deviceType: "web-server"},
                { name: 'r4pvwb1048-vipa02', ip: '10.68.5.108', url: 'token.1dc.com', deviceType: "web-server"}
            ]
        },
        {
            name: "Internal LB DR",
            ip: "10.77.196.73",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Application  restricted zone - A2-R",
            servers: [
                { name: 'r5pvwb1039-vipa02', ip: '10.77.7.52', url: 'token.1dc.com', deviceType: "web-server"},
                { name: 'r5pvwb1040-vipa02', ip: '10.77.7.54', url: 'token.1dc.com', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Tokenizer client net access

        {
            url: 'token.1dc.com',
            ip: "170.186.103.144",
            zone: "Client Net",
            comment: 'RHI used to failover to DR site.<br>'+
                "RHI switches NAT for the client net IP <br>" +
                "to the DR load balancer IP when primary is not available.",
            primary: {
                ip: "10.82.250.31",
                internal_nat: "10.68.196.79",
                comment: "ClientNet LB VIP<br>LB ticket 8883 - completed",
            },
            dr: {
                ip: "10.83.250.31",
                internal_nat: "10.77.196.73",
                comment: "ClientNet LB DR VIP<br>LB ticket 8883 - completed",
            }
        },

        {
            zone: "VISECA",
            url: 'token.1dc.com',
            comment: "VISECA source NAT: 10.86.67.192/26",
            link: {
                ip: "170.186.103.144"
            }
        },

        { zone: "Client Net", url: 'token2.1dc.com', ip: '170.186.105.88', error: 'to be removed'},

        {
            zone: "FD Net",
            url: 'token.1dc.com',
            comment: 'Unused',
            class: 'unused',
            primary: { url: 'token1.1dc.com', ip: '10.68.37.113', class: 'unused', comment: 'DNS record to be removed - REQ000002733754'},
            dr: { url: 'token2.1dc.com', ip: '10.77.37.113', class: 'unused', comment: 'DNS record to be removed - REQ000002733754'}
        },
        { name: 'a4pvwb001-vipaY', ip: '10.68.37.113', url: 'token.1dc.com', class: 'unused', comment: 'to be removed', zone: "Application  restricted zone - A2-R", deviceType: "web-server"},
        { name: 'a5fvwb001-vipaY', ip: '10.77.37.113', url: 'token.1dc.com', class: 'unused', comment: 'to be removed', zone: "Application  restricted zone - A2-R", deviceType: "web-server"},



    ]
};
