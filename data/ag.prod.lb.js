var lbSetup = {
    desc: "AppGuard PROD web app access setup",

    nodes: [

        // ===========================================================
        // PROD servers
        {
            url: "sso.firstdata.com",
            zone: "Internet",
            primary: { url: 'sso.firstdata.com', ip: '217.73.36.87', internal_nat: '10.74.66.43'},
            dr: { url: 'sso-dr.firstdata.com', ip: '217.73.38.87', internal_nat: '10.72.66.43'},
        },
        {
            zone: "Internet",
            name: 'WAF',
            url: "sso.firstdata.com",
            class: 'modify',
            primary: { url: 'sso.firstdata.com', ip: '217.73.36.177', internal_nat: '10.74.80.28'},

        },
        {
            name: "External LB",
            ip: "10.74.66.43",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'n4pvwb1010', ip: '10.74.68.128', url: 'sso.firstdata.com', deviceType: "web-server"},
                { name: 'n4pvwb1011', ip: '10.74.68.129', url: 'sso.firstdata.com', deviceType: "web-server"},
            ]
        },
        {
            name: "External LB DR",
            ip: "10.72.66.43",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'n5pvwb1008', ip: '10.72.68.126', url: 'sso.firstdata.com', deviceType: "web-server"},
                { name: 'n5pvwb1009', ip: '10.72.68.127', url: 'sso.firstdata.com', deviceType: "web-server"},
            ]
        },
        {
            url: "sso.1dc.com",
            zone: "FD Net",
            primary: { url: 'sso.1dc.com', ip: '10.74.66.43'},
            dr: { url: 'sso-dr.1dc.com', ip: '10.72.66.43'},
        },


    ]};
