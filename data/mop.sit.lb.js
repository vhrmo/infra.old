var lbSetup = {
    desc: "MOP & Tokenizer SIT web app access setup",

    classes: [
        {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [

        // ===========================================================
        // Internal access
        {
            zone: "FD Net",
            url: 'mop-i.1dc.com',
            type: 'wildfly',
            contexts: [
                'mop/fdi',
                'mop/tc',
                'mop/aibms',
                'mop/fdms',
                'mop/fda',
                'tokenizer'
            ],
            servers: [
                {
                    name: 'r4cvwb1026-vipa01',
                    ip: '10.68.7.57',
                    url: 'tokenq.1dc.com',
                    zone: "Application restricted zone - A2-R",
                    deviceType: "web-server"
                }
            ]
        },


    ]
};
