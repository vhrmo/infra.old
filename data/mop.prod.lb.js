var lbSetup = {
    desc: "MOP PROD web app access setup",

    classes: [
        // {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [
        // ===========================================================
        // Internet connectivity
        {
            zone: "Internet",
            url: 'mop.firstdata.eu',
            type: 'wildfly',
            contexts: [
                "mop/fdi"
            ],
            primary: { url: 'mope.firstdata.eu', ip: '217.73.36.17', internal_nat: '10.74.66.17'},
            dr: { url: 'dr-mope.firstdata.eu', ip: '217.73.38.17', internal_nat: '10.72.66.17'}
        },
        {
            name: "External LB",
            ip: "10.74.66.17",
            type: 'wildfly',
            deviceType: 'load-balancer',
            // warning: 'sticky sessions seem not to work on LB - check',
            zone: "Internet facing DMZ - W1-R zone",
            comment: "Set up in SSL_BRIDGE mode - means all TLS traffic<br> and certificates are terminated on our web servers.",
            servers: [
                { name: 'r4pvwb1045-vipa01', ip: '10.74.74.90', url: 'mop.firstdata.eu', comment: 'web server is load balancing<br> accross the two app servers', deviceType: "web-server"},
                { name: 'r4pvwb1046-vipa01', ip: '10.74.74.96', url: 'mop.firstdata.eu', comment: 'web server is load balancing<br> accross the two app servers', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB",
            ip: "10.72.66.17",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: "Set up in SSL_BRIDGE mode - means all TLS traffic<br> and certificates are terminated on our web servers.",
            servers: [
                { name: 'r5pvwb1037-vipa01', ip: '10.72.74.84', url: 'mop.firstdata.eu', deviceType: "web-server"},
                { name: 'r5pvwb1038-vipa01', ip: '10.72.74.90', url: 'mop.firstdata.eu', deviceType: "web-server"}
            ]
        },


        // ===========================================================
        // AIBMS Internet connectivity
        {
            zone: "Internet",
            url: 'insight.aibms.com',
            type: 'wildfly',
            comment: 'Managed by third party',
            primary: { url: 'aibmope.firstdata.eu', ip: '217.73.36.19', internal_nat: '10.74.66.18'},
            dr: { url: 'dr-aibmope.firstdata.eu', ip: '217.73.38.19', internal_nat: '10.72.66.18'}
        },
        {
            name: "External LB",
            ip: "10.74.66.18",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: "Set up in SSL_BRIDGE mode - means all TLS traffic<br> and certificates are terminated on our web servers.",
            servers: [
                { name: 'r4pvwb1045-vipa02', ip: '10.74.74.91', url: 'insight.aibms.com', deviceType: "web-server"},
                { name: 'r4pvwb1046-vipa02', ip: '10.74.74.97', url: 'insight.aibms.com', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB",
            ip: "10.72.66.18",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: "Set up in SSL_BRIDGE mode - means all TLS traffic<br> and certificates are terminated on our web servers.",
            servers: [
                { name: 'r5pvwb1037-vipa02', ip: '10.72.74.85', url: 'insight.aibms.com', deviceType: "web-server"},
                { name: 'r5pvwb1038-vipa02', ip: '10.72.74.91', url: 'insight.aibms.com', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // mop.telecash.de new Internet connectivity
        {
            zone: "Internet",
            url: 'mop.telecash.de,mop2.telecash.de',
            type: 'wildfly',
            primary: { url: 'esh-mop2.telecash.de', ip: '217.73.36.125', internal_nat: '10.74.66.54'},
            dr: { url: 'eqx-mop2.telecash.de', ip: '217.73.38.16', internal_nat: '10.72.66.32'}
        },
        {
            name: "External LB",
            ip: "10.74.66.54",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r4pvwb1045-vipa04', ip: '10.74.74.93', url: 'mop.telecash.de', deviceType: "web-server"},
                { name: 'r4pvwb1046-vipa04', ip: '10.74.74.99', url: 'mop.telecash.de', deviceType: "web-server"},
            ]
        },
        {
            name: "External LB",
            ip: "10.72.66.32",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r5pvwb1037-vipa04', ip: '10.72.74.87', url: 'mop.telecash.de', deviceType: "web-server"},
                { name: 'r5pvwb1038-vipa04', ip: '10.72.74.93', url: 'mop.telecash.de', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // accountmanager.firstdatams.com new Internet connectivity
        {
            zone: "Internet",
            url: 'accountmanager.firstdatams.com,accountmanager2.firstdatams.com',
            type: 'wildfly',
            primary: { url: 'esh-accountmanager2.firstdatams.com', ip: '217.73.36.220', internal_nat: '10.74.66.180'},
            dr: { url: 'equ-accountmanager2.firstdatams.com', ip: '217.73.38.220', internal_nat: '10.72.66.180'}
        },
        {
            name: "External LB",
            ip: "10.74.66.180",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r4pvwb1045-vipa03', ip: '10.74.74.92', url: 'accountmanager.firstdatams.com', deviceType: "web-server"},
                { name: 'r4pvwb1046-vipa03', ip: '10.74.74.98', url: 'accountmanager.firstdatams.com', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB",
            ip: "10.72.66.180",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r5pvwb1037-vipa03', ip: '10.72.74.86', url: 'accountmanager.firstdatams.com', deviceType: "web-server"},
                { name: 'r5pvwb1038-vipa03', ip: '10.72.74.92', url: 'accountmanager.firstdatams.com', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // merchantportal.firstdata.at new Internet connectivity
        {
            zone: "Internet",
            url: 'merchantportal.firstdata.at,merchantportal2.firstdata.at',
            type: 'wildfly',
            primary: { url: 'esh-merchantportal2.firstdata.at', ip: '217.73.36.221', internal_nat: '10.74.66.181'},
            dr: { url: 'equ-merchantportal2.firstdata.at', ip: '217.73.38.221', internal_nat: '10.72.66.181'}
        },
        {
            name: "External LB",
            ip: "10.74.66.181",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r4pvwb1045-vipa05', ip: '10.74.74.94', url: 'merchantportal.firstdata.at', deviceType: "web-server"},
                { name: 'r4pvwb1046-vipa05', ip: '10.74.74.100', url: 'merchantportal.firstdata.at', deviceType: "web-server"}
            ]
        },
        {
            name: "External LB",
            ip: "10.72.66.181",
            type: 'wildfly',
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            comment: 'SSL terminated on LB',
            servers: [
                { name: 'r5pvwb1037-vipa05', ip: '10.72.74.88', url: 'merchantportal.firstdata.at', deviceType: "web-server"},
                { name: 'r5pvwb1038-vipa05', ip: '10.72.74.94', url: 'merchantportal.firstdata.at', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // mop.privatbank1891.com Internet connectivity
        {
            zone: "Internet",
            url: 'mop.privatbank1891.com',
            type: 'wildfly',
            class: 'unused',
            comment: 'In this case no LB was done. <br>' +
            'PROD and DR URL are explicity accessible via Internet. <br>' +
            'DR switch is done by privatbank = netm<br><br>' +
            'Client cancelled contract with FD. <BR>' +
            'This URL will be decomissioned by end of 2018',
            primary: { url: 'mop.privatbank1891.firstdata.eu', ip: '217.73.36.92', internal_nat: '10.74.68.20', warning: 'disabled', class: 'unused'},
            dr: { url: 'mop-dr.privatbank1891.firstdata.eu', ip: '217.73.38.92', internal_nat: ' 10.72.68.20', warning: 'disabled', class: 'unused'}
        },
        { name: 'n4pvwb1013-vipa06', ip: '10.74.68.20', url: 'mop.privatbank1891.firstdata.eu', warning: 'stopped', class: 'unused', deviceType: "web-server"},
        { name: 'n5pvwb1010-vipa06', ip: '10.72.68.20', url: 'mop.privatbank1891.com', warning: 'stopped', class: 'unused', deviceType: "web-server"},

        // ===========================================================
        // Internal access
        {
            zone: "FD Net",
            url: 'mop.firstdata.eu',
            type: 'wildfly',
            contexts: [
                'mop/fdi',
                'mop/tc',
                'mop/aibms',
                'mop/fdms',
                'mop/fda',
            ],
            primary: { url: 'mop-pr.firstdata.eu', ip: '10.74.66.17x'},
            dr: { url: 'mop-dr.firstdata.eu', ip: '10.72.66.17x'}
        },
        {
            name: "External LB",
            ip: "10.74.66.17x",
            class: 'duplicate',
            comment: "Internally mop.firstdata.eu resolves to the external LB VIPAs.<br>There is no reason to have separate internal web site for MOP."
        },
        {
            name: "External LB DR",
            ip: "10.72.66.17x",
            class: 'duplicate',
            comment: "Internally mop.firstdata.eu resolves to the external LB VIPAs.<br>There is no reason to have separate internal web site for MOP."
        },

        {
            zone: "FD Net",
            url: 'mop.firstdata.eu',
            comment: 'Migrated to Wildfly',
            class: 'unused',
            primary: { url: 'mopi1.firstdata.eu', ip: '10.68.37.112', class: 'unused', warning:'to be removed'},
            dr: { url: 'mopi2.firstdata.eu', ip: '10.77.37.112', class: 'unused', warning:'to be removed'}
        },
        { name: 'a4pvwb001-vipaX', ip: '10.68.37.112', url: 'mop.firstdata.eu', class: 'unused'},
        { name: 'a5fvwb001-vipaX', ip: '10.77.37.112', url: 'mop.firstdata.eu', class: 'unused'},


    ]
};
