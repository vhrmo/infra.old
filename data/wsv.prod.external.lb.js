var lbSetup = {
    desc: "WSV PROD External web app access setup",

    classes: [
        {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [
        // ===========================================================
        // New external URL to be used for Wildfly
        {
            url: "online.firstdata.com",
            zone: "Internet",
            type: "wildfly",
            comment: "To be used to host new Wildfly apps. <br> Automated failover to DR by Global Site Selector.",
            contexts: [
                'login',
                'gans',
                'esp',
                'edp'
            ],
            primary: {url: "online-esh.firstdata.com", ip: "217.73.36.202", internal_nat: "10.74.66.163", error:"Not working yet", type: 'None'},
            dr: {url: "online-equ.firstdata.com", ip: "217.73.38.202", internal_nat: "10.72.66.164"}
        },
        {
            name: "ONLINE-FIRSTDATA-COM-VIP",
            url: 'online-esh.firstdata.com',
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            ip: "10.74.66.163",
            error: "Not working yet",
            servers: [
                {name: "n4pvwb009-vipa02", ip: "10.74.68.99", url: "online.firstdata.eu", comment: 'Apache 2.4 to be installed', class: 'modify', deviceType: "web-server"},
                {name: "n4pvwb010-vipa02", ip: "10.74.68.100", url: "online.firstdata.eu", comment: 'Apache 2.4 to be installed', class: 'modify', deviceType: "web-server"}
            ]
        },
        {
            name: "ONLINE-FIRSTDATA-COM-VIP",
            url: 'online-equ.firstdata.com',
            zone: "Internet facing DMZ - W1-R zone",
            ip: "10.72.66.164",
            type: 'wildfly',
            deviceType: 'load-balancer',
            servers: [
                {name: "n5bvwb994-vipa02", ip: "10.72.68.105", url: "online.firstdata.eu", comment: 'Apache 2.4', deviceType: "web-server"},
                {name: "n5bvwb995-vipa02", ip: "10.72.68.104", url: "online.firstdata.eu", comment: 'Apache 2.4', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // External online.firstdata.de link
        {
            url: "online.firstdata.de",
            zone: "Internet",
            comment: 'To be used to host old JBoss4 apps until decommissioned. <br>Automated failover to DR by Global Site Selector.',
            contexts: [
                'login',
                // 'ews',
                'dwhmis/distribution'
                ],
            type: 'jboss4',
            primary: {url: "online.firstdata.de", ip: "217.73.36.47", internal_nat: "10.74.66.51"},
            dr: {url: "online-dr.firstdata.de", ip: "217.73.38.47", internal_nat: "10.72.66.52"}
        },
        {
            url: "prod-wsv-online.1dc.com",
            name: "External LB",
            ip: "10.74.66.51",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                {name: "n4pvwb009-vipa01", ip: "10.74.68.97", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n4pvwb010-vipa01", ip: "10.74.68.98", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },
        {
            url: "dr-wsv-online.1dc.com",
            name: "External LB DR",
            ip: "10.72.66.52",
            zone: "Internet facing DMZ - W1-R zone",
            type: 'jboss4',
            deviceType: 'load-balancer',
            servers: [
                {name: "n5bvwb994-vipa01", ip: "10.72.68.101", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n5bvwb995-vipa01", ip: "10.72.68.100", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Cognos link
        {
            url: "bi.firstdata.com",
            zone: "Internet",
            comment: "Cognos URL. Cognos is using WSV web servers <br> which forward the traffic to Cognos Windows based app servers.",
            primary: {url: "bi.firstdata.com", ip: "217.73.36.59", internal_nat: "10.74.66.56"},
            dr: {url: "bi-dr.firstdata.com", ip: "217.73.38.60", internal_nat: "10.72.66.57"}
        },
        {
            url: "bi.firstdata.com",
            name: "External LB",
            ip: "10.74.66.56",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                {name: "n4pvwb009-vipa03", ip: "10.74.68.106", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n4pvwb010-vipa03", ip: "10.74.68.107", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },
        {
            url: "bi.firstdata.com",
            name: "External LB DR",
            ip: "10.72.66.57",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                {name: "n5bvwb994-vipa03", ip: "10.72.68.113", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n5bvwb995-vipa03", ip: "10.72.68.112", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },

    ]};
