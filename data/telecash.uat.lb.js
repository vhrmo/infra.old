var wsvLBSetup = lbSetup = {
    desc: "Telecash UAT web app access setup (KUFO & PolControl)",

    nodes: [

        {
            url: "online-cat.telecash.de",
            zone: "Internet",
            contexts: [
                'login',
                'kufoWeb',
                'polcontrol'
            ],
            primary: {url: "online-cat.telecash.de", ip: "217.73.36.82", internal_nat: "10.74.66.71"}
        },
        {
            name: "ONLINE-CAT-TELECASH-DE-VIP",
            ip: "10.74.66.71",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                { name: 'n4qvwb997-vipa02', ip: '10.74.68.119', url: 'online-cat.telecash.de', deviceType: "web-server"},
            ]
        }
    ]};
