var wsvLBSetup = lbSetup = {
    desc: "Telecash UAT web app access setup (KUFO & PolControl)",

    nodes: [

        {
            url: "online-i.telecash.1dc.com",
            zone: "FD Net",
            contexts: [
                'login',
                'kufoWeb',
                'polcontrol'
            ],
            primary: {url: "online-i.telecash.1dc.com", ip: "10.77.17.230"}
        },

        { name: 'n5dvwb993-vipa02', ip: '10.77.17.230', url: 'online-i.telecash.1dc.com', zone: "Application non-restricted zone - A2-NR", deviceType: "web-server"},
    ]};
