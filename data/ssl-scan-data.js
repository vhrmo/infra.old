ssl_scan_data = {
    "10.77.17.230": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.119": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.120": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.37.113": {
        "-ssl2": "DISABLED",
        "-ssl3": "ENABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "DISABLED"
    },
    "10.68.5.106": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.108": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.249": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.250": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.252": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.253": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.254": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.5.255": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.1": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.2": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.25": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.27": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.3": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.4": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.75": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.6.76": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.68.7.57": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.100": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.101": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.102": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.103": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.104": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.105": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.111": {
        "-ssl2": "DISABLED",
        "-ssl3": "ENABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.112": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.113": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.126": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.127": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.68.44": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.48": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.52": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.54": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.84": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.85": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.86": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.87": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.88": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.90": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.91": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.92": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.93": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.72.74.94": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.100": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.101": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.104": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.106": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.107": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.118": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.128": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.129": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.130": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.131": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.97": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.98": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.68.99": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.100": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.48": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.50": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.52": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.54": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.90": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.91": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.92": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.93": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.94": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.96": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.97": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.98": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.74.74.99": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.17.162": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.17.163": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.17.167": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.17.168": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.17.238": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.37.113": {
        "-ssl2": "DISABLED",
        "-ssl3": "ENABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "DISABLED"
    },
    "10.77.5.154": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.184": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.185": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.186": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.187": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.188": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.189": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.190": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.191": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.192": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.193": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.194": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.195": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.196": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.197": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.211": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.212": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.213": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.219": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.220": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.221": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.5.222": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.6.78": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.6.79": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.6.80": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.6.81": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "ENABLED",
        "-tls1_1": "ENABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.7.52": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    },
    "10.77.7.54": {
        "-ssl2": "DISABLED",
        "-ssl3": "DISABLED",
        "-tls1": "DISABLED",
        "-tls1_1": "DISABLED",
        "-tls1_2": "ENABLED"
    }
}

