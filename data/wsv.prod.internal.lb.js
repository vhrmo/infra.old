var lbSetup = {
    desc: "WSV PROD Internal web app access setup",

    classes: [
        {name: "default", style: "font-family:verdana,text-align:center;"}
    ],

    nodes: [


        // ===========================================================
        // New internal URL to be used for Wildfly
        {
            id: "online.1dc.com",
            url: "online.1dc.com",
            zone: "FD Net",
            type: "wildfly",
            contexts: [
                'login',
                'amos',
                'awis_edoks',
                'awissb',
                'bv',
                'class',
                'edp',
                'esp',
                'espservice',
                'gans'
            ],
            primary: {url: "esh-online.1dc.com", ip: "10.68.196.12"},
            dr: {url: "eqx-online.1dc.com", ip: "10.77.196.17"}
        },
        {
            url: "online.1dc.com",
            ip: "10.68.196.12",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n4pvwb011-vipa05", ip: "10.68.5.254", url: "online.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"},
                {name: "n4pvwb012-vipa05", ip: "10.68.6.3", url: "online.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"}
            ]
        },
        {
            url: "online.1dc.com",
            ip: "10.77.196.17",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n5bvwb992-vipa05", ip: "10.77.5.196", url: "online.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"},
                {name: "n5bvwb993-vipa05", ip: "10.77.5.191", url: "online.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Internal online.firstdata.de link
        {
            url: "online.firstdata.de",
            zone: "FD Net",
            contexts: [
                'login',
                'awis_edoks',
                'awissb',
                // 'ews',
                'webADV',
                'dwhmis'
            ],
            comment: 'To be used to host old JBoss4 apps until decommissioned. <br>Manual failover to DR by DNS alias change by dis.virtual team.',
            type: "jboss4",
            primary: {url: "online.firstdata.de", ip: "10.68.196.41", error: 'TLS1 enabled!'},
            dr: {url: "online-dr.firstdata.de", ip: "10.77.196.41", error: 'TLS1 enabled!'}
        },
        {
            url: "prod-wsv-int-online.1dc.com",
            name: "Internal LB",
            ip: "10.68.196.41",
            type:'jboss4',
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n4pvwb011-vipa01", ip: "10.68.5.249", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n4pvwb012-vipa01", ip: "10.68.5.250", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },
        {
            url: "dr-wsv-int-online.1dc.com",
            name: "Internal LB DR",
            ip: "10.77.196.41",
            type:'jboss4',
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n5bvwb992-vipa01", ip: "10.77.5.185", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n5bvwb993-vipa01", ip: "10.77.5.184", url: "online.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Internal Cognos link
        {
            url: "bi.1dc.com",
            zone: "FD Net",
            comment: "Cognos URL. Cognos is using WSV web servers <br> which forward the traffic to Cognos Windows based app servers.",
            primary: {url: "bi.1dc.com", ip: "10.68.196.56"},
            dr: {url: "bi-dr.1dc.com", ip: "10.77.196.56"}
        },
        {
            url: "bi.firstdata.com",
            name: "BI Internal LB",
            ip: "10.68.196.56",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n4pvwb011-vipa09", ip: "10.68.6.75", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n4pvwb012-vipa09", ip: "10.68.6.76", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },
        {
            url: "bi.firstdata.com",
            name: "BI Internal LB DR",
            deviceType: 'load-balancer',
            ip: "10.77.196.56",
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n5bvwb992-vipa09", ip: "10.77.5.222", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n5bvwb993-vipa09", ip: "10.77.5.221", url: "bi.firstdata.com", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Internal BCS link
        {
            url: "online.bcs.firstdata.de",
            zone: "FD Net",
            contexts: [
                'login',
                'gans',
                'edp'
            ],
            comment: "Bayern card services link to WSV apps. <br><br>" +
                "online.bcs.firstdata.de is a cname DNS record to online.1dc.com<br>" +
                "This means it will resolve to same IP as the online.1dc.com<br><br>" +

                "Users access this URL from Citrix. <br> " +
                "This means they are on the internal FD network.",
            link: {
                id: 'online.1dc.com',
            },
            // primary: {url: "online.bcs.firstdata.de", ip: "10.68.196.44"},
            // dr: {url: "online.bcs.firstdata.de - DR", ip: "10.77.196.44"}
        },
        {
            name: "BCS Internal LB",
            url: "online.bcs.firstdata.de",
            ip: "10.68.196.44",
            type:'jboss4',
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            class: 'modify',
            comment: 'VIP Disabled by disabling all TLS versions incl. TLS1.2',
            warning: "To be deleted",
            servers: [
                {name: "n4pvwb011-vipa03", ip: "10.68.5.252", url: "online.bcs.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"},
                {name: "n4pvwb012-vipa03", ip: "10.68.6.1", url: "online.bcs.firstdata.de", comment: 'Apache 2.2', deviceType: "web-server"}
            ]
        },
        {
            name: "BCS Internal LB DR",
            url: "online.bcs.firstdata.de",
            ip: "10.77.196.44",
            type: "wildfly",
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            comment: "VIP disabled by setting invalid healthcheck URL.",
            class: 'modify',
            warning: "To be deleted",
            servers: [
                {name: "n5bvwb992-vipa03", ip: "10.77.5.194", url: "online.bcs.firstdata.de", comment: 'Apache 2.4', deviceType: "web-server"},
                {name: "n5bvwb993-vipa03", ip: "10.77.5.189", url: "online.bcs.firstdata.de", comment: 'Apache 2.4', deviceType: "web-server"}
            ]
        },


        // ===========================================================
        // Temp URL to be removed
        {
            url: "online.fdi.1dc.com",
            zone: "FD Net",
            type: 'jboss4',
            class: "modify",
            comment: "This should be decommissioned",
            primary: {url: "online.fdi.1dc.com", ip: "10.68.196.57", class: "modify"},
            dr: {url: "online.fdi.1dc.com - DR", ip: "10.77.196.57", class: "modify"}
        },
        {
            name: "Online FDI LB",
            url: "online.fdi.1dc.com",
            ip: "10.68.196.57",
            type:'jboss4',
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n4pvwb011-vipa08", ip: "10.68.6.25", url: "online.fdi.1dc.com", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>Kufo, PolControl, Cognos, Old MOP, old WSV", deviceType: "web-server"},
                {name: "n4pvwb012-vipa08", ip: "10.68.6.27", url: "online.fdi.1dc.com", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>Kufo, PolControl, Cognos, Old MOP, old WSV", deviceType: "web-server"}
            ]
        },
        {
            name: "Online FDI LB DR",
            url: "online.fdi.1dc.com",
            ip: "10.77.196.57",
            type:'jboss4',
            deviceType: 'load-balancer',
            zone: "Application restricted zone - A2-R",
            servers: [
                {name: "n5bvwb993-vipa08", ip: "10.77.5.211", url: "online.fdi.1dc.com", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>Kufo, PolControl, Cognos, Old MOP, old WSV", deviceType: "web-server"},
                {name: "n5bvwb992-vipa08", ip: "10.77.5.213", url: "online.fdi.1dc.com", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>Kufo, PolControl, Cognos, Old MOP, old WSV", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // New URL for BS Services
        // {
        //     url: "bs.firstdata.de",
        //     zone: "FD Net",
        //     comment: 'Deleted',
        //     class: 'unused',
        //     primary: {url: "bs-pr.firstdata.de", ip: "10.68.196.130", comment: 'Deleted by request 1304', class: 'unused'},
        //     dr: {url: "bs-dr.firstdata.de", ip: "10.77.196.130", comment: 'Deleted by request 1304', class: 'unused'}
        // },
        // {
        //     name: 'BS-FIRSTDATA-DE-443-VIP',
        //     zone: "Application restricted zone - A2-R",
        //     ip: "10.68.196.130",
        //     comment: 'Deleted by request 1304',
        //     class: 'unused',
        //     servers: [
        //         {name: "n4pvwb011-vipa04", ip: "10.68.5.253", url: "bs.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"},
        //         {name: "n4pvwb012-vipa04", ip: "10.68.6.2", url: "bs.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"}
        //     ]
        // },
        // {
        //     name: 'BS-DR-FIRSTDATA-DE-443-VIP',
        //     zone: "Application restricted zone - A2-R",
        //     ip: "10.77.196.130",
        //     comment: 'Deleted by request 1373',
        //     class: 'unused',
        //     servers: [
        //         {name: "n5bvwb992-vipa04", ip: "10.77.5.195", url: "bs.firstdata.de", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>Kufo, PolControl, Cognos, Old MOP, old WSV", warning: "TLS1 enabled", deviceType: "web-server"},
        //         {name: "n5bvwb993-vipa04", ip: "10.77.5.190", url: "bs.firstdata.de", deviceType: "web-server"}
        //     ]
        // },
        // ===========================================================
        // New URL for BS Services
        {
            url: "bs.1dc.com",
            zone: "FD Net",
            type: 'wildfly',
            contexts: ["BSServices"],
            primary: {url: "esh-bs.1dc.com", ip: "10.68.196.13"},
            dr: {url: "eqx-bs.1dc.com", ip: "10.77.196.18"}
        },
        {
            name: 'BS-1DC-COM-VIP',
            zone: "Application restricted zone - A2-R",
            ip: "10.68.196.13",
            type: 'wildfly',
            deviceType: 'load-balancer',
            servers: [
                {name: "n4pvwb011-vipa04", ip: "10.68.5.253", url: "bs.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"},
                {name: "n4pvwb012-vipa04", ip: "10.68.6.2", url: "bs.1dc.com", comment: 'Apache 2.4', deviceType: "web-server"}
            ]
        },
        {
            name: 'BS-1DC-COM-VIP',
            zone: "Application restricted zone - A2-R",
            ip: "10.77.196.18",
            type: 'wildfly',
            deviceType: 'load-balancer',
            servers: [
                {name: "n5bvwb992-vipa04", ip: "10.77.5.195", url: "bs.1dc.com", deviceType: "web-server", comment: "Used as an endpoint for BSServices for apps not supporting TLS1.2 <br>but working ok with Wildfly: Kufo, PolControl, old WSV", warning: "TLS1 enabled"},
                // {name: "n5bvwb992-vipa04", ip: "10.77.5.195", url: "bs.1dc.com"},
                {name: "n5bvwb993-vipa04", ip: "10.77.5.190", url: "bs.1dc.com", deviceType: "web-server"}
            ]
        },

        // ===========================================================
        // Unused VIPAs
        {
            name: "Unused preparations for",
            url: "*.firstdata.eu",
            zone: "Application restricted zone - A2-R",
            class: "unused",
            servers: [
                {name: "n4pvwb011-vipa06", ip: "10.68.5.255", url: "online.bcs.firstdata.eu", deviceType: "web-server"},
                {name: "n4pvwb012-vipa06", ip: "10.68.6.4", url: "online.bcs.firstdata.eu", deviceType: "web-server"},
            ]
        },
        {
            name: "Unused preparations for",
            url: "*.firstdata.eu",
            zone: "Application restricted zone - A2-R",
            class: "unused",
            servers: [
                {name: "n5bvwb992-vipa06", ip: "10.77.5.197", url: "online.bcs.firstdata.eu", deviceType: "web-server"},
                {name: "n5bvwb993-vipa06", ip: "10.77.5.192", url: "online.bcs.firstdata.eu", deviceType: "web-server"},
                {name: "n5bvwb992-vipa07", ip: "10.77.5.212", url: "online-erp.firstdata.eu", deviceType: "web-server"}
            ]
        }
    ]};
