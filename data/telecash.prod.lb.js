var wsvLBSetup = lbSetup = {
    desc: "Telecash PROD & UAT web app access setup (KUFO & PolControl)",

    nodes: [
        {
            url: "online.telecash.de",
            zone: "Internet",
            contexts: [
                'login',
                'kufoWeb',
                'polcontrol'
            ],
            primary: {url: "online.telecash.de", ip: "217.73.36.81", internal_nat: "10.74.66.70"},
            dr: {url: "online-dr.telecash.de", ip: "217.73.38.81", internal_nat: "10.72.66.70"}
        },
        {
            name: "External LB",
            ip: "10.74.66.70",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                { name: 'n4pvwb015-vipa02', ip: '10.74.68.118', url: 'online.telecash.de', deviceType: "web-server"},
            ]
        },
        {
            name: "External LB DR",
            ip: "10.72.66.70",
            zone: "Internet facing DMZ - W1-R zone",
            deviceType: 'load-balancer',
            servers: [
                { name: 'n5bvwb991-vipa02', ip: '10.72.68.120', url: 'online.telecash.de', deviceType: "web-server"},
            ]
        },

        {
            url: "barvus-m.telecash.de",
            zone: "Internet",
            contexts: [
                'login',
                'polcontrol'
            ],
            comment: "Police NRW requires dedicated URL which works only in the NRW police network.<br> That is why all users in NRW knows only barvus-m.telecash.de.",
            primary: {url: "barvus-m.telecash.de", ip: "217.73.36.119", internal_nat: "10.74.66.85"},
            dr: {url: "barvus-m-dr.telecash.de", ip: "217.73.38.119", internal_nat: "10.72.66.85"}
        },
        {
            name: "BARVUS-M-TELECASH-DE-443-VIP",
            ip: "10.74.66.85",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'n4pvwb1019-vipa03', ip: '10.74.68.101', url: 'barvus-m.telecash.de', deviceType: "web-server"},
                { name: 'n4pvwb015-vipa02', ip: '10.74.68.118', url: 'online.telecash.de<br>barvus-m.telecash.de', warning: 'Two sites pointing to the same vipa', deviceType: "web-server"},
            ]
        },
        {
            name: "BARVUS-M-DR-TELECASH-DE-443-VIP",
            ip: "10.72.66.85",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                { name: 'n5bvwb1004-vipa03', ip: '10.72.68.44', url: 'barvus-m.telecash.de', warning: 'n5bvwb1004-vipa03 resolves incorrectly to 10.72.68.45', deviceType: "web-server"},
                { name: 'n5bvwb991-vipa02', ip: '10.72.68.120', url: 'online.telecash.de<br>barvus-m.telecash.de', warning: 'Two sites pointing to the same vipa', deviceType: "web-server"},
            ]
        },

        {
            url: "partner.telecash.de",
            zone: "Internet",
            error: 'Not working, to be cleaned up',
            primary: {url: "partner.telecash.de", ip: "217.73.33.60", internal_nat: "???"},
        },

    ]};
