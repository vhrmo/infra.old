var lbSetup = {
    desc: "MISC SIT web app access setup",

    nodes: [


        // ===========================================================
        // SIT servers
        {
            url: "misc-i.firstdata.eu",
            zone: "FD Net",
            type: 'jboss4',
            contexts: [
                'login',
                'mandat',
                // 'merchant-migration/app',
                'CurrencyCalculator/fremdwaehrungskurse',
                'CurrencyCalculator/hvbkurse'
            ],
            primary: {url: 'misc-i.firstdata.eu', ip: '10.77.17.200'},
        },
        {
            name: 'n5dvwb994-vipa01',
            ip: '10.77.17.200',
            url: 'misc-i.firstdata.eu',
            deviceType: "web-server",
            zone: 'EQX-D2-NR'
        },


    ]
};
