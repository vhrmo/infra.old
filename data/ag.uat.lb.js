var lbSetup = {
    desc: "AppGuard UAT web app access setup",

    nodes: [

        // ===========================================================
        // UAT servers
        {
            url: "cat-sso.firstdata.com",
            zone: "Internet",
            servers: [
                {url: 'cat-sso.firstdata.com', ip: '217.73.36.89', internal_nat: '10.74.66.42'},
                {
                    url: 'cat-sso.firstdata.com',
                    ip: '217.73.36.178',
                    internal_nat: '10.74.80.29',
                    warning: 'This is being tested as <br>a replacement for WAF & LB',
                },
            ],
        },
        {
            zone: "Internet facing DMZ - W1-R zone",
            name: 'WAF',
            ip: '10.74.80.29',
            // link: {ip: "10.74.66.42"}
            warning: 'This is being tested as <br>a replacement for WAF & LB',
            servers: [
                {name: 'n4cvwb1010', ip: '10.74.68.130', url: 'cat-sso.firstdata.com', deviceType: "web-server"},
                {name: 'n4cvwb1011', ip: '10.74.68.131', url: 'cat-sso.firstdata.com', deviceType: "web-server"},
            ]
        },
        {
            name: "External LB",
            ip: "10.74.66.42",
            deviceType: 'load-balancer',
            zone: "Internet facing DMZ - W1-R zone",
            servers: [
                {name: 'n4cvwb1010', ip: '10.74.68.130', url: 'cat-sso.firstdata.com', deviceType: "web-server"},
                {name: 'n4cvwb1011', ip: '10.74.68.131', url: 'cat-sso.firstdata.com', deviceType: "web-server"},
            ]
        },
        {
            url: "cat-sso.1dc.com",
            zone: "FD Net",
            primary: {url: 'cat-sso.1dc.com', ip: '10.74.66.42'},
        },


    ]};

