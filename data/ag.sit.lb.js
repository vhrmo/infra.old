var lbSetup = {
    desc: "AppGuard SIT web app access setup",

    nodes: [

        // ===========================================================
        // SIT servers
        {
            url: "sit-sso.1dc.com, sit.sso.1dc.com",
            zone: "FD Net",
            primary: {url: 'sit-sso.1dc.com', ip: '10.77.17.238'},
        },
        {
            name: 'n5dvwb1002',
            ip: '10.77.17.238',
            url: 'sit.sso.1dc.com',
            deviceType: "web-server",
            zone: 'EQX-D2-NR'
        },


    ]
};
