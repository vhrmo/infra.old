
const TreeTableHeader = () => {
  return (
      <thead>
      <tr>
        <th>Name</th>
        <th>Job</th>
        <th></th>
      </tr>
      </thead>
  )
};

class TreeTableRow extends React.Component {
  state = {
    expanded: false
  };

  toggle = () => {
    if (this.props.rowData.children) {
      this.setState({expanded: !this.state.expanded});
    }
  }

  render() {
    const rowData = (
        <tr>
          <td style={{paddingLeft:this.props.level*50}}>{this.props.rowData.name}</td>
          <td>{this.props.rowData.job}</td>
          <td onClick={this.toggle}>
            {(this.props.rowData.children ? this.state.expanded ? "Expanded" : "Expand" : "")}
          </td>
        </tr>
    );

    // print children if expanded
    const children = !this.state.expanded ? <React.Fragment></React.Fragment> : this.props.rowData.children.map((child, index) => {
      return <TreeTableRow key={index} rowData={child} level={this.props.level+1}/>
    });

    // print details if expanded
    const detail = !this.state.expanded ? <React.Fragment></React.Fragment> : (
        <tr>
          <td colSpan="3">bla</td>
        </tr>
    );

    return (
        <React.Fragment>{rowData}{children}</React.Fragment>
    );
  }
}

const TreeTableBody = props => {
  const body = props.data.map((row, index) => {
    return (
        <TreeTableRow key={index} rowData={row} level={0}/>
    )
  });
  return (
      <tbody>{body}</tbody>
  )
};

class TreeTable extends React.Component {

  render() {

    const {data} = this.props;

    return (
        <div className="container">
          <table>
            <TreeTableHeader/>
            <TreeTableBody data={data}/>
          </table>
        </div>
    )
  }

}


class TreeTableDemo extends React.Component {

  state = {
    data: [
      {
        name: 'Charlie',
        job: 'Janitor',
        description: "Some test description ..."
      },
      {
        name: 'Mac',
        job: 'Bouncer',
        description: "Some more test description ...",
        children: [
          {
            name: 'Mac',
            job: 'Bouncer',
            description: "Some more test description ..."
          },
          {
            name: 'Dee',
            job: 'Aspring actress',
            description: "Even more test description ...",
            children: [
              {
                name: 'Mac',
                job: 'Bouncer',
                description: "Some more test description ..."
              },
              {
                name: 'Dee',
                job: 'Aspring actress',
                description: "Even more test description ..."
              },
              {
                name: 'Dennis',
                job: 'Bartender',
                description: "Bla bla bla ...."
              },
            ]

          },
          {
            name: 'Dennis',
            job: 'Bartender',
            description: "Bla bla bla ...."
          },
        ]
      },
      {
        name: 'Dee',
        job: 'Aspring actress',
        description: "Even more test description ..."
      },
      {
        name: 'Dennis',
        job: 'Bartender',
        description: "Bla bla bla ...."
      },
    ],
  };

  render() {

    const {data} = this.state;

    return (
        <TreeTable data={data}/>
    );
  }
}



ReactDOM.render(<TreeTableDemo />, document.getElementById('root'));
